#include <Arduino.h>

const int BLUE_LED = 2;
volatile int interruptCount = 0;
volatile bool interruptPending = false;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

void IRAM_ATTR onTimer2() {
    portENTER_CRITICAL_ISR(&timerMux);
    interruptPending = true;
    portEXIT_CRITICAL_ISR(&timerMux);
}

void setup2() {
    // Serial is a static object in the Arduino lib that uses UART1, 
    // tied to the USB port in the esp32doit-devkit-v1 breakout board
    Serial.begin(115200);
    Serial.printf("\n\n\nprogram started\n");

    // GPIO_2 is connected to the blue LED in the main board
    pinMode(BLUE_LED, OUTPUT);
    digitalWrite(BLUE_LED, 0);

    // f=80MHz, make tick freq = 80MHz/8000 = 10KHz, period=100 uSec 
    hw_timer_t * timer = timerBegin(0, 8000, true);
    timerAttachInterrupt(timer, &onTimer2, true);

    // hit on onTimer() 1 out of 1000 ticks, so every 100 mSec
    timerAlarmWrite(timer, 1000, true);
    timerAlarmEnable(timer);
}

void loop2() {

    bool interruptHappened = false;
    portENTER_CRITICAL(&timerMux);
    if (interruptPending) {
        interruptPending = false;
        interruptHappened = true;
    }
    portEXIT_CRITICAL(&timerMux);

    if ( interruptHappened ) {
        digitalWrite(BLUE_LED, !digitalRead(BLUE_LED));
        Serial.printf("interrupt count:%d\n", ++interruptCount);
    }
}
