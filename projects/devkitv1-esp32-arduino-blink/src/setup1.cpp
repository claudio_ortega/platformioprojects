#include <Arduino.h>
#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"

const int BLUE_LED = 2;

void IRAM_ATTR onTimer1() {
    for (int i=0; i<2; i++) {
        digitalWrite(BLUE_LED, 1);   
        digitalWrite(BLUE_LED, 0);   // takes 260 nSec
    }
}

void setup1() {

    //disable brownout detection
    WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); 

    // GPIO_2 is connected to the blue LED in the main board
    pinMode(BLUE_LED, OUTPUT);
    digitalWrite(BLUE_LED, 0);

    // f=80MHz, make tick freq = 80MHz/8 = 10KHz, period=100 nSec 
    hw_timer_t * timer = timerBegin(0, 8, true);
    timerAttachInterrupt(timer, &onTimer1, true);

    // hit on onTimer() 1 out of 50 ticks, so every 5 uSec
    timerAlarmWrite(timer, 30, true);
    timerAlarmEnable(timer);
}

void loop1() {
}
