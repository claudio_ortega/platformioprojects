#include <Arduino.h>
#include <avr/io.h>
#include <util/delay.h>

// make the LED pin an output for PB2
void setup() {
    DDRB |= (1 << DDB2);
    Serial.begin(115200);
    Serial.println("setup()");
}

void toggleLEDPin() {
    static int ledOn = 0;

    ledOn = ( ledOn == 0 ) ? 1 : 0;
    if ( ledOn == 1 ) {
        PORTB |= 1 << PB2;
    }
    else {
        PORTB &= ~( 1 << PB2 );
    }
}

void loop() {
  toggleLEDPin();
  delay(100);
}
