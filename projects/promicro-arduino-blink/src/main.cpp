#include <Arduino.h>

const int RX_LED_PIN = 17;

void setup() {
  Serial.begin(115200);
  pinMode(RX_LED_PIN, OUTPUT);
  Serial.println("Start");
}

static int i = 0;
static String dummy = "";

void loop() {
  // Serial.println will light up the TX LED, see photo/schematics
  Serial.println(dummy + "loop:" + i++);

  // corresponds with the RX LED on the board, see photo/schematics
  // all LEDs in the board light up on a low level
  // note: the schematic in this directory is probably wrong,
  // as it does not show pin 17 going into an LED
  digitalWrite(RX_LED_PIN, LOW);
  delay(100);
  digitalWrite(RX_LED_PIN, HIGH);
  delay(1000);
}