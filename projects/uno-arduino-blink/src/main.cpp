#include <Arduino.h>
#define GREENLED 13

void setup() {
  Serial.begin(115200);
  pinMode(GREENLED, OUTPUT);
  Serial.print("Start");
}

void loop() {
  digitalWrite(GREENLED, LOW);
  Serial.print("led off");
  delay(500);
  digitalWrite(GREENLED, HIGH);
  Serial.print("led on");
  delay(500);
}