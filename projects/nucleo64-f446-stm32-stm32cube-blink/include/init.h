#ifndef __INIT_H
#define __INIT_H

#ifdef __cplusplus
extern "C" {
#endif

void Error_Handler();
void SystemClock_Config();
void MX_USART2_UART_Init();
void MX_GPIO_Init();
void Nucleo_BSP_Init();

#ifdef __cplusplus
}
#endif

#endif   // __INIT_H
