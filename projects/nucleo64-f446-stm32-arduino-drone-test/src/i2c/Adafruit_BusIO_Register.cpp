#include <i2c/Adafruit_BusIO_Register.h>

/*!
 *    @brief  Create a register we access over an I2C or SPI Device. This is a
 * handy function because we can pass in NULL for the unused interface, allowing
 * libraries to mass-define all the registers
 *    @param  i2cdevice The I2CDevice to use for underlying I2C access, if NULL
 * we use SPI
 *    @param  spidevice The SPIDevice to use for underlying SPI access, if NULL
 * we use I2C
 *    @param  reg_addr The address pointer value for the I2C/SMBus/SPI register,
 * can be 8 or 16 bits
 *    @param  type     The method we use to read/write data to SPI (which is not
 * as well defined as I2C)
 *    @param  width    The width of the register data itself, defaults to 1 byte
 *    @param  byteorder The byte order of the register (used when width is > 1),
 * defaults to LSBFIRST
 *    @param  address_width The width of the register address itself, defaults
 * to 1 byte
 */
Adafruit_BusIO_Register::Adafruit_BusIO_Register(
    Adafruit_I2CDevice *i2cdevice,
    uint16_t reg_addr,
    uint8_t width,
    uint8_t byteorder,
    uint8_t address_width) {

    _i2cdevice = i2cdevice;
    _addrwidth = address_width;
    _address = reg_addr;
    _byteorder = byteorder;
    _width = width;
    _cachedValue = 0;
    _isCachedValue = false;
    memset(_buffer, 0, sizeof(_buffer));
}

/*!
 *    @brief  Write a buffer of data to the register location
 *    @param  buffer Pointer to data to write
 *    @param  len Number of bytes to write
 *    @return True on successful write (only really useful for I2C as SPI is
 * uncheckable)
 */
bool Adafruit_BusIO_Register::write(uint8_t *buffer, uint8_t len) {

    if ( _i2cdevice == nullptr ) {
        return false;
    }

    uint8_t addrbuffer[2] = {
        (uint8_t)(_address & 0xFF),
        (uint8_t)(_address >> 8)
    };

    return _i2cdevice->write(buffer, len, true, addrbuffer, _addrwidth);
}

/*!
 *    @brief  Write up to 4 bytes of data to the register location
 *    @param  value Data to write
 *    @param  numbytes How many bytes from 'value' to write
 *    @return True on successful write (only really useful for I2C as SPI is
 * uncheckable)
 */
bool Adafruit_BusIO_Register::write(uint32_t value, uint8_t numbytes) {

    if ( _i2cdevice == nullptr ) {
        return false;
    }
  
    if (numbytes > 4 || numbytes < 1) {
        return false;
    }

    for (int i = 0; i < numbytes; i++) {
        if (_byteorder == LSBFIRST) {
            _buffer[i] = value & 0xFF;
        } else {
            _buffer[numbytes - i - 1] = value & 0xFF;
        }
        value >>= 8;
    }

    _cachedValue = value;
    _isCachedValue = true;

    return write(_buffer, numbytes);
}

/*!
 *    @brief  Read data from the register location. This does not do any error checking!
 *    @return Returns FALSE on failure
 */
bool Adafruit_BusIO_Register::read(uint32_t* out) {
   
    if ( _i2cdevice == nullptr ) {
        return false;
    }

    if (!read(_buffer, _width)) {
        return false;
    }

    uint32_t value = 0;

    for (int i = 0; i < _width; i++) {
        value <<= 8;
        if (_byteorder == LSBFIRST) {
            value |= _buffer[_width-i-1];
        } else {
            value |= _buffer[i];
        }
    }

    *out = value;

    return true;
}

/*!
 *    @brief  Read a buffer of data from the register location
 *    @param  buffer Pointer to data to read into
 *    @param  len Number of bytes to read
 *    @return True on successful write (only really useful for I2C as SPI is uncheckable)
 */
bool Adafruit_BusIO_Register::read(uint8_t *buffer, uint8_t len) {

    if ( _i2cdevice == nullptr ) {
        return false;
    }

    uint8_t addrbuffer[2] = {
        (uint8_t)(_address & 0xFF),
        (uint8_t)(_address >> 8)
    };

    return _i2cdevice->write_then_read(addrbuffer, _addrwidth, buffer, len, false);
}

/*!
 *    @brief  Read 2 bytes of data from the register location
 *    @param  value Pointer to uint16_t variable to read into
 *    @return True on successful write (only really useful for I2C as SPI is
 * uncheckable)
 */
bool Adafruit_BusIO_Register::read(uint16_t *value) {
    if (!read(_buffer, 2)) {
        return false;
    }

    if (_byteorder == LSBFIRST) {
        *value = _buffer[1];
        *value <<= 8;
        *value |= _buffer[0];
    } else {
        *value = _buffer[0];
        *value <<= 8;
        *value |= _buffer[1];
    }

    return true;
}

/*!
 *    @brief  Read 1 byte of data from the register location
 *    @param  value Pointer to uint8_t variable to read into
 *    @return True on successful write (only really useful for I2C as SPI is
 * uncheckable)
 */
bool Adafruit_BusIO_Register::read(uint8_t *value) {
    if (!read(_buffer, 1)) {
        return false;
    }

    *value = _buffer[0];
    return true;
}

/*!
 *    @brief  Create a slice of the register that we can address without
 * touching other bits
 *    @param  reg The Adafruit_BusIO_Register which defines the bus/register
 *    @param  bits The number of bits wide we are slicing
 *    @param  shift The number of bits that our bit-slice is shifted from LSB
 *      ie: say the register bit7,bit6...bit1,bit0 has a value 0x05
 *      then bits,shift --> result
 *              (1, 0) --> 1   (bit0)
 *              (2, 0) --> 1   (bit1,bit0)
 *              (3, 0) --> 5   (bit2,bit1,bit0)
 *              (2, 1) --> 2   (bit2,bit1)
 *              (2, 2) --> 1   (bit3,bit2)
 */
Adafruit_BusIO_RegisterBits::Adafruit_BusIO_RegisterBits(
    Adafruit_BusIO_Register *reg, 
    uint8_t bits, 
    uint8_t shift) {
    _register = reg;
    _bits = bits;
    _shift = shift;
}

/*!
 *    @brief  Read 4 bytes of data from the register
 *    @return  data The 4 bytes to read
 */
bool Adafruit_BusIO_RegisterBits::read(uint32_t* in) {

    uint32_t val = 0;

    bool b1 = _register->read(&val);
    if ( !b1 ) {
        return false;
    }

    val >>= _shift;
    *in = val & ((1 << (_bits)) - 1);

    return true;
}

/*!
 *    @brief  Write 4 bytes of data to the register
 *    @param  data The 4 bytes to write
 *    @return True on successful write (only really useful for I2C as SPI is
 * uncheckable)
 */
bool Adafruit_BusIO_RegisterBits::write(uint32_t data) {
    
    uint32_t val = 0;
    
    if ( ! _register->read(&val) ) {
        return false;
    }

    // mask off the data before writing
    uint32_t mask = (1 << (_bits)) - 1;
    data &= mask;

    mask <<= _shift;
    val &= ~mask;          // remove the current data at that spot
    val |= data << _shift; // and add in the new data

    return _register->write(val, _register->width());
}

/*!
 *    @brief  The width of the register data, helpful for doing calculations
 *    @returns The data width used when initializing the register
 */
uint8_t Adafruit_BusIO_Register::width() { return _width; }

/*!
 *    @brief  Set the default width of data
 *    @param width the default width of data read from register
 */
void Adafruit_BusIO_Register::setWidth(uint8_t width) { _width = width; }

/*!
 *    @brief  Set register address
 *    @param address the address from register
 */
void Adafruit_BusIO_Register::setAddress(uint16_t address) {
  _address = address;
}

/*!
 *    @brief  Set the width of register address
 *    @param address_width the width for register address
 */
void Adafruit_BusIO_Register::setAddressWidth(uint16_t address_width) {
  _addrwidth = address_width;
}

