/*
  TwoWire2.h - TWI/I2C library for Arduino & Wiring
  Copyright (c) 2006 Nicholas Zambetti.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  Modified 2012 by Todd Krein (todd@krein.org) to implement repeated starts
*/

#ifndef TwoWire2_h
#define TwoWire2_h

#include "Stream.h"
#include "Arduino.h"
extern "C" {
    #include "utility/twi.h"
}


class TwoWire2 {
    public:
        static const uint8_t MASTER_ADDRESS = 0x01;

        TwoWire2(uint8_t sda, uint8_t scl);
        void begin(uint8_t address, bool generalCall, int clockInHz);
        void end();
        void beginTransmission(uint8_t);
        uint8_t endTransmission(bool);
        uint8_t requestFrom(uint8_t, uint8_t, uint8_t);
        size_t write(uint8_t);
        size_t write(const uint8_t *, size_t);
        int available();
        int read();
        int peek();
        void flush();
        
    private:
        uint8_t *rxBuffer;
        uint8_t rxBufferAllocated;
        uint8_t rxBufferIndex;
        uint8_t rxBufferLength;

        uint8_t txAddress;
        uint8_t *txBuffer;
        uint8_t txBufferAllocated;
        uint8_t txBufferIndex;
        uint8_t txBufferLength;

        bool transmitting;
        i2c_t _i2c;

        bool allocateRxBuffer(size_t length);
        bool allocateTxBuffer(size_t length);
        void resetRxBuffer();
        void resetTxBuffer();
        uint8_t requestFrom(uint8_t, uint8_t, uint32_t, uint8_t, uint8_t);
};

#endif
