/*!
 *  @file Adafruit_LSM6DS.h
 *
 * 	I2C Driver base for Adafruit LSM6DSxx 6-DoF Accelerometer and Gyroscope
 *      library
 *
 * 	Adafruit invests time and resources providing this open source code,
 *      please support Adafruit and open-source hardware by purchasing products
 *from Adafruit!
 *
 *	BSD license (see license.txt)
 */

#ifndef _ADAFRUIT_LSM6DS_H
#define _ADAFRUIT_LSM6DS_H

#include <Arduino.h>
#include "i2c/Adafruit_BusIO_Register.h"
#include "i2c/Adafruit_I2CDevice.h"
#include "i2c/Wire2.h"

/** The accelerometer data rate */
typedef enum data_rate {
  LSM6DS_RATE_SHUTDOWN,
  LSM6DS_RATE_12_5_HZ,
  LSM6DS_RATE_26_HZ,
  LSM6DS_RATE_52_HZ,
  LSM6DS_RATE_104_HZ,
  LSM6DS_RATE_208_HZ,
  LSM6DS_RATE_416_HZ,
  LSM6DS_RATE_833_HZ,
  LSM6DS_RATE_1_66K_HZ,
  LSM6DS_RATE_3_33K_HZ,
  LSM6DS_RATE_6_66K_HZ,
} lsm6ds_data_rate_t;

/** The accelerometer data range */
typedef enum accel_range {
  LSM6DS_ACCEL_RANGE_2_G,
  LSM6DS_ACCEL_RANGE_16_G,
  LSM6DS_ACCEL_RANGE_4_G,
  LSM6DS_ACCEL_RANGE_8_G
} lsm6ds_accel_range_t;

/** The gyro data range */
typedef enum gyro_range {
  LSM6DS_GYRO_RANGE_125_DPS = 0b0010,
  LSM6DS_GYRO_RANGE_250_DPS = 0b0000,
  LSM6DS_GYRO_RANGE_500_DPS = 0b0100,
  LSM6DS_GYRO_RANGE_1000_DPS = 0b1000,
  LSM6DS_GYRO_RANGE_2000_DPS = 0b1100,
  ISM330DHCX_GYRO_RANGE_4000_DPS = 0b0001
} lsm6ds_gyro_range_t;

/** The high pass filter bandwidth */
typedef enum hpf_range {
  LSM6DS_HPF_ODR_DIV_50 = 0,
  LSM6DS_HPF_ODR_DIV_100 = 1,
  LSM6DS_HPF_ODR_DIV_9 = 2,
  LSM6DS_HPF_ODR_DIV_400 = 3,
} lsm6ds_hp_filter_t;

typedef struct {
    float x;
    float y;
    float z;
} xyz_triplet_t;

class Adafruit_LSM6DS {
    public:
        static const uint8_t LSM6DS_I2CADDR_PRIMARY = 0x6A;
        static const uint8_t LSM6DS_I2CADDR_SECONDARY = 0x6B;

        Adafruit_LSM6DS();
        virtual ~Adafruit_LSM6DS();

        bool begin_I2C(uint8_t i2c_addr, TwoWire2 *wire);
        bool reset();
        bool init();

        bool pollRegisters();
        void getGyro( xyz_triplet_t* omega );
        void getAccel( xyz_triplet_t* accel );

        bool setAccelDataRate(lsm6ds_data_rate_t data_rate);

        bool setAccelRange(lsm6ds_accel_range_t new_range);

        bool setGyroDataRate(lsm6ds_data_rate_t data_rate);

        bool setGyroRange(lsm6ds_gyro_range_t new_range);

        void configIntOutputs(bool active_low, bool open_drain);
        void configInt1(
            bool drdy_temp, 
            bool drdy_g, 
            bool drdy_xl,
            bool step_detect, 
            bool wakeup);
        void configInt2(
            bool drdy_temp, 
            bool drdy_g, 
            bool drdy_xl);
        void highPassFilter(
            bool enabled, 
            lsm6ds_hp_filter_t filter);

        bool accelerationAvailable();
        bool gyroscopeAvailable();

        bool chipID(uint32_t* in);
        bool status(uint32_t* in);
        
    protected:
        Adafruit_I2CDevice *i2c_dev; 

    private:

        bool pollRawRegisters();
        // last raw reading
        int16_t rawTemp;
        int16_t rawAccX;
        int16_t rawAccY;
        int16_t rawAccZ;
        int16_t rawGyroX;
        int16_t rawGyroY;
        int16_t rawGyroZ;

        void scaleRawRegisters();
        // last reading's accelerometer m/s^2
        float accX;
        float accY;
        float accZ;
        float gyroX;
        float gyroY;
        float gyroZ;

        lsm6ds_gyro_range_t gyro_range;
        lsm6ds_accel_range_t accel_range;

        float gyro_scale;
        float accel_scale;
};

#endif
