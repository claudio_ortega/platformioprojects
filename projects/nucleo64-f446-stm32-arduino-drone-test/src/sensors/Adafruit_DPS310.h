/**************************************************************************/
/*!
    @file     Adafruit_DPS310.h
    @author   Limor Fried (Adafruit Industries)
    ----> https://www.adafruit.com/4494

    Adafruit invests time and resources providing this open source code,
    please support Adafruit and open-source hardware by purchasing
    products from Adafruit!

*/
/**************************************************************************/

#ifndef ADAFRUIT_DPS310_H
#define ADAFRUIT_DPS310_H

#include <Arduino.h>
#include "i2c/Adafruit_BusIO_Register.h"
#include "i2c/Adafruit_I2CDevice.h"
#include "i2c/Wire2.h"

/** The measurement rate ranges */
typedef enum {
  DPS310_1HZ = 0,   ///< 1 Hz
  DPS310_2HZ = 1,   ///< 2 Hz
  DPS310_4HZ = 2,   ///< 4 Hz
  DPS310_8HZ = 3,   ///< 8 Hz
  DPS310_16HZ = 4,  ///< 16 Hz
  DPS310_32HZ = 5,  ///< 32 Hz
  DPS310_64HZ = 6,  ///< 64 Hz
  DPS310_128HZ = 7, ///< 128 Hz
} dps310_rate_t;

/** The  oversample rate ranges */
typedef enum {
  DPS310_1SAMPLE = 0,    ///< 1 Hz
  DPS310_2SAMPLES = 1,   ///< 2 Hz
  DPS310_4SAMPLES = 2,   ///< 4 Hz
  DPS310_8SAMPLES = 3,   ///< 8 Hz
  DPS310_16SAMPLES = 4,  ///< 16 Hz
  DPS310_32SAMPLES = 5,  ///< 32 Hz
  DPS310_64SAMPLES = 6,  ///< 64 Hz
  DPS310_128SAMPLES = 7, ///< 128 Hz
} dps310_oversample_t;

/** The  oversample rate ranges */
typedef enum {
  DPS310_IDLE = 0b000,            ///< Stopped/idle
  DPS310_ONE_PRESSURE = 0b001,    ///< Take single pressure measurement
  DPS310_ONE_TEMPERATURE = 0b010, ///< Take single temperature measurement
  DPS310_CONT_PRESSURE = 0b101,   ///< Continuous pressure measurements
  DPS310_CONT_TEMP = 0b110,       ///< Continuous pressure measurements
  DPS310_CONT_PRESTEMP = 0b111,   ///< Continuous temp+pressure measurements
} dps310_mode_t;

typedef struct {
    float temperature;
    float pressure;
} dps310_sensor_data;

/** Class for hardware interfacing with a DPS310 */
class Adafruit_DPS310 {
    public:
        static const uint8_t DPS310_I2CADDR_PRIMARY = 0x77; 
        static const uint8_t DPS310_I2CADDR_SECONDARY = 0x76; 

        Adafruit_DPS310();
        ~Adafruit_DPS310();

        bool begin_I2C(uint8_t i2c_addr, TwoWire2 *wire);
        bool init();
        bool readAndLogRegisters();

        bool readMeasurements(dps310_sensor_data* data);

    private:
        bool setMode(dps310_mode_t mode);
        bool readCalibration();
        bool reset();
        bool readBitFromRegister(uint8_t registerId, uint8_t bitposition, uint8_t *out);
        bool configurePressure(dps310_rate_t rate, dps310_oversample_t os);
        bool configureTemperature(dps310_rate_t rate, dps310_oversample_t os);
        bool sensorAvailable();
        bool calibrationAvailable();
        bool pressureAvailable();
        bool temperatureAvailable();

        int32_t c0, c1, c01, c11, c20, c21, c30, c00, c10;
        int32_t _raw_pressure, _raw_temperature;
        float _pressure_scale, _temperature_scale;
        float _pressure, _temperature;
        Adafruit_I2CDevice *i2c_dev = nullptr;
};

#endif
