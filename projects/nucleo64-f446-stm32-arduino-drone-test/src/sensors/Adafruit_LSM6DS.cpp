
/*!
 *  @file Adafruit_LSM6DS.cpp Adafruit LSM6DS 6-DoF Accelerometer
 *  and Gyroscope library
 *
 *  @section intro_sec Introduction
 *
 * 	I2C Driver base for Adafruit LSM6DS 6-DoF Accelerometer
 *      and Gyroscope libraries
 *
 * 	Adafruit invests time and resources providing this open source code,
 *  please support Adafruit and open-source hardware by purchasing products from
 * 	Adafruit!
 *
 *  @section dependencies Dependencies
 *  This library depends on the Adafruit BusIO library
 *
 *  This library depends on the Adafruit Unified Sensor library

 *  @section author Author
 *
 *  Bryan Siepert for Adafruit Industries
 *
 * 	@section license License
 *
 * 	BSD (see license.txt)
 *
 * 	@section  HISTORY
 *
 *     v1.0 - First release
 */

#include <Arduino.h>
#include "sensors/Adafruit_LSM6DS.h"

#define LSM6DS_FUNC_CFG_ACCESS 0x1 ///< Enable embedded functions register
#define LSM6DS_INT1_CTRL 0x0D      ///< Interrupt control for INT 1
#define LSM6DS_INT2_CTRL 0x0E      ///< Interrupt control for INT 2
#define LSM6DS_WHOAMI 0xF          ///< Chip ID register
#define LSM6DS_CTRL1_XL 0x10       ///< Main accelerometer config register
#define LSM6DS_CTRL2_G 0x11        ///< Main gyro config register
#define LSM6DS_CTRL3_C 0x12        ///< Main configuration register
#define LSM6DS_CTRL8_XL 0x17       ///< High and low pass for accel
#define LSM6DS_CTRL10_C 0x19       ///< Main configuration register
#define LSM6DS_WAKEUP_SRC 0x1B     ///< Why we woke up
#define LSM6DS_STATUS_REG 0X1E     ///< Status register
#define LSM6DS_OUT_TEMP_L 0x20     ///< First data register (temperature low)
#define LSM6DS_OUTX_L_G 0x22       ///< First gyro data register
#define LSM6DS_OUTX_L_A 0x28       ///< First accel data register
#define LSM6DS_STEPCOUNTER 0x4B    ///< 16-bit step counter
#define LSM6DS_TAP_CFG 0x58        ///< Tap/pedometer configuration
#define LSM6DS_WAKEUP_THS 0x5B     ///< Single and double-tap function threshold register
#define LSM6DS_WAKEUP_DUR 0x5C     ///< Free-fall, wakeup, timestamp and sleep mode duration
#define LSM6DS_MD1_CFG 0x5E        ///< Functions routing on INT1 register

static const float _data_rate_arr[] = {
    [LSM6DS_RATE_SHUTDOWN] = 0.0f,    
    [LSM6DS_RATE_12_5_HZ] = 12.5f,
    [LSM6DS_RATE_26_HZ] = 26.0f,      
    [LSM6DS_RATE_52_HZ] = 52.0f,
    [LSM6DS_RATE_104_HZ] = 104.0f,    
    [LSM6DS_RATE_208_HZ] = 208.0f,
    [LSM6DS_RATE_416_HZ] = 416.0f,    
    [LSM6DS_RATE_833_HZ] = 833.0f,
    [LSM6DS_RATE_1_66K_HZ] = 1660.0f, 
    [LSM6DS_RATE_3_33K_HZ] = 3330.0f,
    [LSM6DS_RATE_6_66K_HZ] = 6660.0f,
};

Adafruit_LSM6DS::Adafruit_LSM6DS() {
    gyro_range = LSM6DS_GYRO_RANGE_125_DPS;
    accel_range = LSM6DS_ACCEL_RANGE_2_G;
    i2c_dev = nullptr;
    gyro_scale = 0;
    accel_scale = 0;
    rawTemp = 0;
    rawAccX = 0;
    rawAccY = 0;
    rawAccZ = 0;
    rawGyroX = 0;
    rawGyroY = 0;
    rawGyroZ = 0;
    accX = 0;
    accY = 0;
    accZ = 0;
    gyroX = 0;
    gyroY = 0;
    gyroZ = 0;
}

Adafruit_LSM6DS::~Adafruit_LSM6DS() {}

bool Adafruit_LSM6DS::chipID(uint32_t* in) {
    return Adafruit_BusIO_Register(
        i2c_dev, 
        LSM6DS_WHOAMI,
        1,
        LSBFIRST,
        1).read(in);
}

/*
 *    @brief  Read Status register
 *    @returns 8 Bit value from Status register
 */
bool Adafruit_LSM6DS::status(uint32_t* in) {
    return Adafruit_BusIO_Register(
        i2c_dev, 
        LSM6DS_STATUS_REG,
        1,
        LSBFIRST,
        1).read(in);
}

/*
 *    @brief  Sets up the hardware and initializes I2C
 *    @param  i2c_address
 *            The I2C address to be used.
 *    @param  wire
 *            The Wire object to be used for I2C connections.
 *    @return True if initialization was successful, otherwise false.
 */
bool Adafruit_LSM6DS::begin_I2C(
    uint8_t i2c_address, 
    TwoWire2 *wire) {
 
    if ( i2c_dev != nullptr ) {
        return false;
    }

    i2c_dev = new Adafruit_I2CDevice(i2c_address, wire);
    
    return true;
}

/*
    @brief Resets the sensor to its power-on state, clearing all registers and memory
*/
bool Adafruit_LSM6DS::reset() {

    Adafruit_BusIO_Register ctrl3 = Adafruit_BusIO_Register(
        i2c_dev, 
        LSM6DS_CTRL3_C,
        1,
        LSBFIRST,
        1);

    Adafruit_BusIO_RegisterBits sw_reset = Adafruit_BusIO_RegisterBits(
        &ctrl3, 
        1, 
        0);

    if ( ! sw_reset.write(0x01) ) {
        return false;
    }

    uint32_t val = 0;
    while ( val != 0 ) {
        bool test = sw_reset.read(&val);
        if ( !test ) {
            return false;
        }
    }

    return true;
}

bool Adafruit_LSM6DS::init() {
    if (!reset() ) {
        return false;
    }

    if (!setAccelRange(LSM6DS_ACCEL_RANGE_8_G)) {
        return false;
    }

    if (!setGyroRange(LSM6DS_GYRO_RANGE_250_DPS ) ) {
        return false;
    }

    if (!setAccelDataRate(LSM6DS_RATE_416_HZ) ) {
        return false;
    }

    if (!setGyroDataRate(LSM6DS_RATE_416_HZ) ) {
        return false;
    }

    return true;
}

void Adafruit_LSM6DS::getGyro( xyz_triplet_t* omega ){
    omega->x = gyroX;
    omega->y = gyroY;
    omega->z = gyroZ;
}

void Adafruit_LSM6DS::getAccel( xyz_triplet_t* accel ) {
    accel->x = accX;
    accel->y = accY;
    accel->z = accZ;
}

bool Adafruit_LSM6DS::setAccelDataRate(lsm6ds_data_rate_t data_rate) {
    Adafruit_BusIO_Register ctrl1 = Adafruit_BusIO_Register(
        i2c_dev, 
        LSM6DS_CTRL1_XL,
        1,
        LSBFIRST,
        1);

    return Adafruit_BusIO_RegisterBits(&ctrl1, 4, 4).write(data_rate);
}

bool Adafruit_LSM6DS::setAccelRange(lsm6ds_accel_range_t accel_range) {
    const float SENSORS_GRAVITY = 9.80665;
    accel_scale = 1; // range is in milli-g per bit!
    if (accel_range == LSM6DS_ACCEL_RANGE_16_G)
        accel_scale = 0.488 * SENSORS_GRAVITY / 1000;
    if (accel_range == LSM6DS_ACCEL_RANGE_8_G)
        accel_scale = 0.244 * SENSORS_GRAVITY / 1000;
    if (accel_range == LSM6DS_ACCEL_RANGE_4_G)
        accel_scale = 0.122 * SENSORS_GRAVITY / 1000;
    if (accel_range == LSM6DS_ACCEL_RANGE_2_G)
        accel_scale = 0.061 * SENSORS_GRAVITY / 1000;
    Adafruit_BusIO_Register ctrl1 = Adafruit_BusIO_Register(
        i2c_dev, 
        LSM6DS_CTRL1_XL,
        1,
        LSBFIRST,
        1);
    return Adafruit_BusIO_RegisterBits(&ctrl1, 2, 2).write(accel_range);
}

bool Adafruit_LSM6DS::setGyroDataRate(lsm6ds_data_rate_t data_rate) {
    Adafruit_BusIO_Register ctrl2 = Adafruit_BusIO_Register(
        i2c_dev, 
        LSM6DS_CTRL2_G,
        1,
        LSBFIRST,
        1);
    return Adafruit_BusIO_RegisterBits(
        &ctrl2, 
        4, 
        4).write(data_rate);
}

bool Adafruit_LSM6DS::setGyroRange(lsm6ds_gyro_range_t gyro_range) {
    const float SENSORS_DPS_TO_RADS = 2.0 * 3.14159265 / 360.0;
    gyro_scale = 1; 
    if (gyro_range == ISM330DHCX_GYRO_RANGE_4000_DPS)
        gyro_scale = 140.0 * SENSORS_DPS_TO_RADS / 1000.0;
    if (gyro_range == LSM6DS_GYRO_RANGE_2000_DPS)
        gyro_scale = 70.0 * SENSORS_DPS_TO_RADS / 1000.0;
    if (gyro_range == LSM6DS_GYRO_RANGE_1000_DPS)
        gyro_scale = 35.0 * SENSORS_DPS_TO_RADS / 1000.0;
    if (gyro_range == LSM6DS_GYRO_RANGE_500_DPS)
        gyro_scale = 17.50 * SENSORS_DPS_TO_RADS / 1000.0;
    if (gyro_range == LSM6DS_GYRO_RANGE_250_DPS)
        gyro_scale = 8.75 * SENSORS_DPS_TO_RADS / 1000.0;
    if (gyro_range == LSM6DS_GYRO_RANGE_125_DPS)
        gyro_scale = 4.375 * SENSORS_DPS_TO_RADS / 1000.0;

    Adafruit_BusIO_Register ctrl2 = Adafruit_BusIO_Register(
        i2c_dev, 
        LSM6DS_CTRL2_G,
        1,
        LSBFIRST,
        1);
    return Adafruit_BusIO_RegisterBits(
        &ctrl2, 
        4, 
        0).write(gyro_range);
}

void Adafruit_LSM6DS::highPassFilter(
    bool filter_enabled,
    lsm6ds_hp_filter_t filter) {
    Adafruit_BusIO_Register ctrl8 = Adafruit_BusIO_Register(
        i2c_dev, 
        LSM6DS_CTRL8_XL,
        1,
        LSBFIRST,
        1);
    Adafruit_BusIO_RegisterBits(&ctrl8, 1, 2).write(filter_enabled);
    Adafruit_BusIO_RegisterBits(&ctrl8, 2, 5).write(filter);
}

bool Adafruit_LSM6DS::pollRegisters() {
    const bool status = pollRawRegisters();
    if (!status) {
        return false;
    }
    scaleRawRegisters();
    return true;
}

bool Adafruit_LSM6DS::pollRawRegisters() {
    static uint8_t buffer[14] = {0};
    memset(buffer, 0, sizeof(buffer));

    const bool status = Adafruit_BusIO_Register(
        i2c_dev, 
        LSM6DS_OUT_TEMP_L, 
        sizeof(buffer),
        LSBFIRST,
        1).read(buffer, sizeof(buffer));

    if ( !status ) {
        return false;
    }

    rawTemp = buffer[0] | (buffer[1] << 8);
    rawGyroX = buffer[2] | (buffer[3] << 8);
    rawGyroY = buffer[4] | (buffer[5] << 8);
    rawGyroZ = buffer[6] | (buffer[7] << 8);
    rawAccX = buffer[8] | (buffer[9] << 8);
    rawAccY = buffer[10] | (buffer[11] << 8);
    rawAccZ = buffer[12] | (buffer[13] << 8);

    return true;
}

void Adafruit_LSM6DS::scaleRawRegisters() {
    accX = rawAccX * accel_scale;
    accY = rawAccY * accel_scale;
    accZ = rawAccZ * accel_scale;
    gyroX = rawGyroX * gyro_scale;
    gyroY = rawGyroY * gyro_scale;
    gyroZ = rawGyroZ * gyro_scale;
}

/*
    @brief Sets the INT1 and INT2 pin activation mode
    @param active_low true to set the pins  as active high, false to set the
                      mode to active low
    @param open_drain true to set the pin mode as open-drain, false to set the
                      mode to push-pull
*/
void Adafruit_LSM6DS::configIntOutputs(bool active_low, bool open_drain) {
    Adafruit_BusIO_Register ctrl3 = Adafruit_BusIO_Register(
        i2c_dev, 
        LSM6DS_CTRL3_C,
        1,
        LSBFIRST,
        1);
    Adafruit_BusIO_RegisterBits(&ctrl3, 2, 4).write((active_low << 1) | open_drain);
}

/*
    @brief Enables and disables the data ready interrupt on INT 1.
    @param drdy_temp true to output the data ready temperature interrupt
    @param drdy_g true to output the data ready gyro interrupt
    @param drdy_xl true to output the data ready accelerometer interrupt
    @param step_detect true to output the step detection interrupt (default off)
    @param wakeup true to output the wake up interrupt (default off)
*/
void Adafruit_LSM6DS::configInt1(
    bool drdy_temp, 
    bool drdy_g, 
    bool drdy_xl,
    bool step_detect, 
    bool wakeup) {

    Adafruit_BusIO_Register(
        i2c_dev, 
        LSM6DS_INT1_CTRL,
        1,
        LSBFIRST,
        1).write((step_detect << 7) | (drdy_temp << 2) | (drdy_g << 1) | drdy_xl, 0);

    Adafruit_BusIO_Register md1cfg = Adafruit_BusIO_Register(
        i2c_dev, 
        LSM6DS_MD1_CFG,
        1,
        LSBFIRST,
        1);

    Adafruit_BusIO_RegisterBits(&md1cfg, 1, 5).write(wakeup);
}

/*
    @brief Enables and disables the data ready interrupt on INT 2.
    @param drdy_temp true to output the data ready temperature interrupt
    @param drdy_g true to output the data ready gyro interrupt
    @param drdy_xl true to output the data ready accelerometer interrupt
*/
void Adafruit_LSM6DS::configInt2(bool drdy_temp, bool drdy_g, bool drdy_xl) {
    Adafruit_BusIO_Register int2_ctrl = Adafruit_BusIO_Register(
        i2c_dev, 
        LSM6DS_INT2_CTRL,
        1,
        LSBFIRST,
        1);

    Adafruit_BusIO_RegisterBits(&int2_ctrl, 3, 0).write((drdy_temp << 2) | (drdy_g << 1) | drdy_xl);
}

bool Adafruit_LSM6DS::accelerationAvailable() {
    uint32_t val = 0;
    if ( !status(&val) ) {
        return false;
    }
    return (val & 0x01) ? true : false;
}

bool Adafruit_LSM6DS::gyroscopeAvailable() {
    uint32_t val = 0;
    if ( !status(&val) ) {
        return false;
    }
    return (val & 0x02) ? true : false;
}

