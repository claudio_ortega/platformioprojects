#include <Arduino.h>
#include "common/logger.hpp"
#include "common/pwm.hpp"  
#include "common/system2.hpp"
#include "common/telemetry.hpp"
#include "test/testAll.hpp"
#include "i2c/Wire2.h"
#include "sensors/Adafruit_DPS310.h"
#include "sensors/Adafruit_LSM6DS.h"
#include "sensors/Adafruit_LIS3MDL.h"
#include <PinNames.h>

namespace testAll {

    bool printOut;
    bool usePressure;
    bool useImu;
    bool useMag;
    bool useUART_RX;
    bool useUART_TX;
    bool usePwm;
    int periodMicroSec;
    int computeSize;
    int telemetryLoops;
    int printLoops;

    HardwareSerial* serialTelemetry1 = nullptr;
    HardwareSerial* serialTelemetry2 = nullptr;
    HardwareSerial* serialTelemetry3 = nullptr;

    HardwareTimer* timer = nullptr;
    TwoWire2* wire = nullptr;
    Adafruit_LSM6DS* lsm6ds = nullptr;
    Adafruit_LIS3MDL* lis3mdl = nullptr;
    Adafruit_DPS310* dps310 = nullptr;
    Arduino_CRC32 *crc32 = nullptr;

    const PinName pwmPins[4] = {PB_4, PB_6, PB_3, PB_5};
    Pwm *pPwm[4] = {nullptr};
    const int pwmFrequency = 1000;
    uint32_t testPin = 0;  

    const int telemetry_tx_size = 18;
    float telemetry_tx[telemetry_tx_size] = {0};

    float* a = nullptr;
    float* b = nullptr;

    inline bool shouldPrint( bool printOut, uint64_t loopCount, int printLoops ) {
        return printOut && ( loopCount % printLoops == 0 );
    }

    static float updateComputation() {
        float sum = 0.0;
        for ( int i=0; i<computeSize; i++ ) {
            sum = sum + (a[i] * b[i]);
        }
        return sum;
    }

    static void updateImu( uint64_t loopCount, float telemetry_tx_payload[] ) {
        static xyz_triplet_t accel = {0}; 
        static xyz_triplet_t gyro = {0}; 
        static uint64_t errCount = 0; 

        const bool status = lsm6ds->pollRegisters();
        if ( !status ) {
            errCount++;
        }

        lsm6ds->getAccel(&accel);
        lsm6ds->getGyro(&gyro);

        if ( shouldPrint( printOut, loopCount, printLoops ) ) {
            logger::print (Serial, "accel->x: [%f], accel->y:[%f], accel->z:[%f]", accel.x, accel.y, accel.z );
            logger::print (Serial, "gyro->x: [%f], gyro->y: [%f], gyro->z: [%f]", gyro.x, gyro.y, gyro.z );
            logger::print (Serial, "errCount: [%d]", errCount );
        }

        telemetry_tx_payload[0] = accel.x;
        telemetry_tx_payload[1] = accel.y;
        telemetry_tx_payload[2] = accel.z;
        
        telemetry_tx_payload[3] = gyro.x;
        telemetry_tx_payload[4] = gyro.y;
        telemetry_tx_payload[5] = gyro.z;        
    }

    static void updateMag( uint64_t loopCount, float telemetry_tx_payload[] ) {
        
        static xyz_mag_triplet mag = {0};
        static uint32_t totalCount = 0; 
        static uint32_t errCount = 0; 

        const bool status = lis3mdl->pollRegisters();  
        if ( !status ) {
            errCount++;
        }
        totalCount++;

        lis3mdl->getMag(&mag);  
        
        if ( shouldPrint( printOut, loopCount, printLoops ) ) {
            logger::print (Serial, "mag->x: [%f], mag->y:[%f], mag->z:[%f]", mag.x, mag.y, mag.z );
            logger::print (Serial, "counters: [%d][%d][%f]", errCount, totalCount, (float)errCount/(float)totalCount );
        }

        telemetry_tx_payload[6] = mag.x;
        telemetry_tx_payload[7] = mag.y;
        telemetry_tx_payload[8] = mag.z;
    }

    static void updatePressure( uint64_t loopCount, float telemetry_tx_payload[] ) {

        static dps310_sensor_data measurement = {0};
        static uint64_t errCount = 0; 
        
        const bool ok = dps310->readMeasurements(&measurement);
        const float altitude = 44330.8 - 4946.54 * pow ( measurement.pressure / 100.0, 0.1902632 );

        if ( !ok ) {
            errCount++;
        }

        if ( shouldPrint( printOut, loopCount, printLoops ) ) {
            logger::print (Serial, "temperature (cent): [%f]", measurement.temperature );
            logger::print (Serial, "pressure (hPa): [%f]", measurement.pressure );
            logger::print (Serial, "altitude (m): [%f]", altitude );
            logger::print (Serial, "errCount: [%d]", errCount );
        }

        telemetry_tx_payload[9] = measurement.temperature;
        telemetry_tx_payload[10] = measurement.pressure;
        telemetry_tx_payload[11] = altitude;
    }

    static void updatePwm( uint64_t loopCount ) {
        const uint32_t widthInMicroSec = loopCount % 1000;
        for ( int i=0; i<4; i++ ) {
            pPwm[i]->setDurationInMicroSec(widthInMicroSec );
        }
    }

    static void updateUART_TX( uint64_t loopCount, float telemetry_tx_payload[] ) {
        if ( loopCount % telemetryLoops == 0 ) {
            telemetry::print( serialTelemetry1, crc32, loopCount/telemetryLoops, telemetry_tx_payload, telemetry_tx_size );
        }
    }

    static void updateUART_RX( uint64_t loopCount ) {
        while( serialTelemetry1->available() ) {
            const uint8_t rx = (const uint8_t) serialTelemetry1->read();
            logger::print (Serial, "rx1:[%x]", rx );
        }

        while( serialTelemetry2->available() ) {
            const uint8_t rx = (const uint8_t) serialTelemetry2->read();
            logger::print (Serial, "rx2:[%x]", rx );
        }

        while( serialTelemetry3->available() ) {
            const uint8_t rx = (const uint8_t) serialTelemetry3->read();
            logger::print (Serial, "rx3:[%x]", rx );
        }
    }

    static void timerCallback() { 

        static uint32_t loopCount = 0;

        loopCount++;

        digitalWrite(testPin, 1);
        
        if (useImu) {
            updateImu(loopCount, telemetry_tx);
        }
       
        if (useMag) {
            updateMag(loopCount, telemetry_tx);
        }
       
        if (usePressure) {
            updatePressure(loopCount, telemetry_tx);
        }
       
        if ( usePwm ) {
            updatePwm(loopCount);
        }
        
        if ( useUART_RX ) {
            updateUART_RX(loopCount);
        }

        if ( useUART_TX ) {
            updateUART_TX(loopCount, telemetry_tx);
        }

        volatile float sum = updateComputation();
        if ( shouldPrint( printOut, loopCount, printLoops ) ) {
            logger::print (
                Serial,
                "loopCount:%d, computeSize:%d, sum:%f",
                loopCount,
                computeSize,
                sum );
        }

        digitalWrite(testPin, 0);
    }

    static void initPwm() {
        for ( int i=0; i<4; i++ ) {
            pPwm[i] = new Pwm();
            pPwm[i]->init(pwmPins[i], pwmFrequency );
        }
    }

    static void initUART( int baudrate ) {
        serialTelemetry1 = new HardwareSerial(PA_10, PA_9);
        serialTelemetry1->begin(baudrate);

        serialTelemetry2 = new HardwareSerial(PC_5, PB_10);
        serialTelemetry2->begin(baudrate);

        serialTelemetry3 = new HardwareSerial(PC_7, PC_6);
        serialTelemetry3->begin(baudrate);

        crc32 = new Arduino_CRC32();
    }

    static bool initImu( TwoWire2* twoWire ) {
        
        lsm6ds = new Adafruit_LSM6DS();

        if (! lsm6ds->begin_I2C(Adafruit_LSM6DS::LSM6DS_I2CADDR_SECONDARY, twoWire)) {         
            return false;
        }

        if (!lsm6ds->init() ) {
            return false;
        }

        return true;
    }
 
    static bool initMag( TwoWire2* twoWire ) {

        lis3mdl = new Adafruit_LIS3MDL();

        if ( !lis3mdl->begin_I2C(Adafruit_LIS3MDL::LIS3MDL_I2CADDR_PRIMARY, twoWire) ) {
            return false;
        }

        if ( !lis3mdl->init() ) {
            return false;
        }

        return true;
    }

    static bool initPressure( TwoWire2* twoWire ) {

        dps310 = new Adafruit_DPS310();

        if ( ! dps310->begin_I2C(Adafruit_DPS310::DPS310_I2CADDR_PRIMARY, twoWire) ) {
            return false;
        }

        if ( ! dps310->init() ) {
            return false;
        }

        return true;
    }

    static void initComputation() {
        a = new float[computeSize];
        b = new float[computeSize];
        for ( int i=0; i<computeSize; i++ ) {
            a[i] = (float)(i+1);
            b[i] = 1.0/a[i];
        }
    }

    static void initTimer( int periodMicroSec ) {
        timer = new HardwareTimer(TIM1);
        timer->setOverflow(periodMicroSec, MICROSEC_FORMAT);
        timer->attachInterrupt(timerCallback);
        timer->resume();
    }

    bool init(
        bool inPrintOut,
        bool inUsePressure,
        bool inUseImu,
        bool inUseMag,
        bool inUseUART_RX,
        bool inUseUART_TX,
        bool inUsePwm,
        int clockI2C,
        int baudRate,
        int inPeriodMicroSec,
        int inComputeSize,
        int inTelemetryLoops,
        int inPrintLoops ) {
            
        printOut = inPrintOut;
        usePressure = inUsePressure;
        useImu = inUseImu;
        useMag = inUseMag;
        useUART_RX = inUseUART_RX;
        useUART_TX = inUseUART_TX;
        usePwm = inUsePwm;
        periodMicroSec = inPeriodMicroSec;
        computeSize = inComputeSize;
        telemetryLoops = inTelemetryLoops;
        printLoops = inPrintLoops;

        testPin = pinNametoDigitalPin(PA_5);
        pinMode(testPin, OUTPUT);

        wire = new TwoWire2(SDA, SCL);

        if ( useImu || useMag || usePressure ) {
            wire->begin(TwoWire2::MASTER_ADDRESS, false, clockI2C);
        }

        int initErrCount = 0;

        if ( useImu ) {
            if ( !initImu( wire ) )  {
                logger::print (Serial, "imu sensor init failed");
                initErrCount++;
            }
        }
       
        if ( useMag ) {
            if ( !initMag( wire ) )  {
                logger::print (Serial, "mag sensor init failed");
                initErrCount++;
            }
        }
       
        if ( usePressure ) {
            if ( !initPressure( wire ) )  {
                logger::print (Serial, "pressure sensor init failed");
                initErrCount++;
            }
        }

        if ( initErrCount > 0 ) {
            logger::print (Serial, "initialization failed");
            return false;
        }

        if ( usePwm ) {
            initPwm();
        }
        
        if ( useUART_RX || useUART_TX ) {
            initUART( baudRate );
        }

        initComputation();

        initTimer( periodMicroSec );

        logger::print (Serial, "initialization succeeded");

        return true;
    }
}
