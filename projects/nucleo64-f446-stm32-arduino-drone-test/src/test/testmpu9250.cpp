#include <Arduino.h>
#include <Wire.h>
#include "common/logger.hpp"
#include "test/testmpu9250.hpp"

namespace testMPU9250_2 {

    typedef struct {
        int16_t x;
        int16_t y;
        int16_t z;
    } omega_t;

    typedef struct {
        int16_t x;
        int16_t y;
        int16_t z;
    } accel_t;

    typedef struct {
        uint8_t x;
        uint8_t y;
        uint8_t z;
    } mag_scale_t;

    typedef struct {
        int8_t x;
        int8_t y;
        int8_t z;
    } mag_t;

    static const int MPU9250_I2C_ADDRESS = 0x68;
    static const int AK8963_I2C_ADDRESS = 0x0c;

    static const int ACCEL_XOUT_H = 0x3B;
    static const int GYRO_XOUT_H = 0x43;
    static const int PWR_MGMT_1 = 0x6B;
    static const int CONFIG = 0x1A;
    static const int GYRO_CONFIG = 0x1B;
    static const int ACCEL_CONFIG_1 = 0x1C;
    static const int ACCEL_CONFIG_2 = 0x1D;

    static HardwareSerial SerialTelemetry1(PA_10, PA_9);
    static HardwareSerial SerialTelemetry2(PC_5, PB_10);
    static HardwareSerial SerialTelemetry3(PC_7, PC_6);

    void update_IT_callback() { 

        {
            Wire.beginTransmission(MPU9250_I2C_ADDRESS);
            Wire.write(ACCEL_XOUT_H);
            Wire.endTransmission();
            Wire.requestFrom(MPU9250_I2C_ADDRESS, 6);

            accel_t accel = {0};
            accel.x = Wire.read()<<8 | Wire.read();
            accel.y = Wire.read()<<8 | Wire.read();
            accel.z = Wire.read()<<8 | Wire.read();
            logger::print (Serial, "accel.x:[%d], accel.y:[%d], accel.z:[%d]", accel.x, accel.y, accel.z );
        }

        {
            Wire.beginTransmission(MPU9250_I2C_ADDRESS);
            Wire.write(GYRO_XOUT_H);
            Wire.endTransmission();
            Wire.requestFrom(MPU9250_I2C_ADDRESS, 6);

            omega_t omega = {0};
            omega.x = Wire.read()<<8 | Wire.read();
            omega.y = Wire.read()<<8 | Wire.read();
            omega.z = Wire.read()<<8 | Wire.read();
            logger::print (Serial, "omega.x:[%d], omega.y:[%d], omega.z:[%d]", omega.x, omega.y, omega.z );
        }

        {
            const static uint8_t x[] = {0x55, 0x00, 0x55};
            SerialTelemetry1.write(x, sizeof(x) );
            SerialTelemetry2.write(x, sizeof(x) );
            SerialTelemetry3.write(x, sizeof(x) );
        }
    }

    void initIMU() {

        Wire.beginTransmission(MPU9250_I2C_ADDRESS);
        Wire.write(PWR_MGMT_1);
        // MSB=1 ==> reset
        Wire.write(0x80);           
        Wire.endTransmission();

        Wire.beginTransmission(MPU9250_I2C_ADDRESS);
        Wire.write(CONFIG);
        // turn on the internal low-pass filter for gyro/temp with 10Hz bandwidth, F_CHOICE should be 0b11 (see next)
        Wire.write(0x05);                     
        Wire.endTransmission();

        Wire.beginTransmission(MPU9250_I2C_ADDRESS);
        Wire.write(GYRO_CONFIG);
        //set the gyro scale to 2000 °/s and F_CHOICE to 0b11, hence F_CHOICE_B to 0b00
        Wire.write(0b11 << 3);                
        Wire.endTransmission();

        Wire.beginTransmission(MPU9250_I2C_ADDRESS);
        Wire.write(ACCEL_CONFIG_1);
        //set the accel scale to 4g
        Wire.write(0b01 << 3);                
        Wire.endTransmission();

        Wire.beginTransmission(MPU9250_I2C_ADDRESS);
        Wire.write(ACCEL_CONFIG_2);
        //  turn on the internal low-pass filter for accel with 460Hz bandwidth
        Wire.write(0x00);                     
        Wire.endTransmission();
    }

    void initTimer() {
        HardwareTimer* const timer = new HardwareTimer(TIM1);
        timer->attachInterrupt(update_IT_callback);
        timer->setOverflow(100, HERTZ_FORMAT); 
        timer->resume();
    }

    void init() {

        SerialTelemetry1.begin(921600); 
        SerialTelemetry2.begin(921600); 
        SerialTelemetry3.begin(9600); 

        Wire.setClock(400000);
        Wire.begin();

        initIMU();
        initTimer();
    }
}
