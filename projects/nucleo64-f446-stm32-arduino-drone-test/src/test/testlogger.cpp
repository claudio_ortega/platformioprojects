#include <Arduino.h>
#include "common/logger.hpp"
#include "test/testlogger.hpp"

namespace testLogger {

    static void timerCallback() { 
        static bool timerCallBackEnteredOnce = false; 
        if ( !timerCallBackEnteredOnce ) {
            timerCallBackEnteredOnce = true;
            return;
        }

        static uint32_t loopCount = 0; 
        if ( loopCount % 100 == 0 ) {
            logger::print (Serial, "(1) loopCount:%d", loopCount );
        }

        loopCount++;
    }

    static void initTimer() {
        HardwareTimer* const timer = new HardwareTimer(TIM1);
        timer->attachInterrupt(timerCallback);
        timer->setOverflow(100, HERTZ_FORMAT); 
        timer->resume();
    }

    static void showOff() {
        const float f1 = 1.0;
        const double d2 = 2.0;
        static int i1 = 1;
        const int i2 = 2;
        const uint64_t l3 = 3L;
        logger::print (Serial, "(1) -------------------------");
        logger::print (Serial, "i1:[%d], i2:[%d], f1:[%f], d2:[%f], l3:[%d]", i1, i2, f1, d2, l3);
        logger::print (Serial, "i1:[%d], i2:[%d], f1:[%f], d2:[%f], l3:[%d]", i1, i2, f1, d2, l3);
        logger::print (Serial, "f0:[%f]", 0.0);
        logger::print (Serial, "f1:[%f]", 0.001);
        logger::print (Serial, "f2:[%f]", 0.012);
        logger::print (Serial, "f3:[%f]", 0.123);
        logger::print (Serial, "f4:[%f]", 1.234);
        logger::print (Serial, "f5:[%f]", 12.345);
        logger::print (Serial, "f6:[%f]", 123.45);
        logger::print (Serial, "f7:[%f]", 1234.5);
        logger::print (Serial, "f8:[%f]", 12245.0);
        logger::print (Serial, "fa:[%f]", 12245.1234);
        logger::print (Serial, "(2) -------------------------");
        logger::print (Serial, "f0:[%f]", -0.0);
        logger::print (Serial, "f1:[%f]", -0.001);
        logger::print (Serial, "f2:[%f]", -0.012);
        logger::print (Serial, "f3:[%f]", -0.123);
        logger::print (Serial, "f4:[%f]", -1.234);
        logger::print (Serial, "f5:[%f]", -12.345);
        logger::print (Serial, "f6:[%f]", -123.45);
        logger::print (Serial, "f7:[%f]", -1234.5);
        logger::print (Serial, "f8:[%f]", -12245.0);
        logger::print (Serial, "fa:[%f]", -12245.1234);
        logger::print (Serial, "(3) -------------------------");
    }

    bool init() {
        showOff();
        initTimer();
        return true;
    }
}
