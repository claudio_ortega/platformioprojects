#include <Arduino.h>
#include "common/logger.hpp"
#include "test/testBlink.hpp"
#include "variant_NUCLEO_F446RE.h"  // PIN defs
#include "pins_arduino.h"           // PIN mappings

namespace testBlink {

    const uint32_t GREEN_LED_ON_BOARD = pinNametoDigitalPin(PA_5);   
    const uint32_t BLUE_BUTTON_ON_BOARD = pinNametoDigitalPin(PC_13);

    void update_IT_callback() { 

        static uint64_t loopCount = 0;
        ++loopCount;

        if ( digitalRead( BLUE_BUTTON_ON_BOARD ) == 0 ) {
            digitalWrite(GREEN_LED_ON_BOARD, 0);
        }
        else {
            digitalWrite(GREEN_LED_ON_BOARD, loopCount%10 == 0);  
        }
    }

    void initTimer() {
        HardwareTimer* const timer = new HardwareTimer(TIM1);
        timer->attachInterrupt(update_IT_callback);
        timer->setOverflow(100000, HERTZ_FORMAT); 
        timer->resume();
    }

    void init() {
        pinMode(BLUE_BUTTON_ON_BOARD, INPUT);
        pinMode(GREEN_LED_ON_BOARD, OUTPUT);
        initTimer();
    }
}
