#include <Arduino.h>
#include "common/logger.hpp"
#include "test/test3uarts.hpp"

namespace test3UARTS {

    static HardwareSerial* serialTelemetry1;
    static HardwareSerial* serialTelemetry2;
    static HardwareSerial* serialTelemetry3;

    void update_IT_callback() { 

        // static uint64_t loopCount = 0;
        // loopCount++;
        // logger::print ( Serial, "update_IT_callback():[%d]", loopCount );

        const static uint8_t x[] = {0x55, 0xaa, 0x55};

        serialTelemetry1->write(x, sizeof(x) );
        serialTelemetry2->write(x, sizeof(x) );
        serialTelemetry3->write(x, sizeof(x) );

        while( serialTelemetry1->available() ) {
            const uint8_t rx = (const uint8_t) serialTelemetry1->read();
            logger::print (Serial, "rx1:[%x]", rx );
        }

        while( serialTelemetry2->available() ) {
            const uint8_t rx = (const uint8_t) serialTelemetry2->read();
            logger::print (Serial, "rx2:[%x]", rx );
        }

        while( serialTelemetry3->available() ) {
            const uint8_t rx = (const uint8_t) serialTelemetry3->read();
            logger::print (Serial, "rx3:[%x]", rx );
        }
    }

    void initTimer() {
        HardwareTimer* const timer = new HardwareTimer(TIM1);
        timer->attachInterrupt(update_IT_callback);
        timer->setOverflow(100, HERTZ_FORMAT); 
        timer->resume();
    }

    void init() {

        serialTelemetry1 = new HardwareSerial(PA_10, PA_9);
        serialTelemetry2 = new HardwareSerial(PC_5, PB_10);
        serialTelemetry3 = new HardwareSerial(PC_7, PC_6);

        serialTelemetry1->begin(921600); 
        serialTelemetry2->begin(921600); 
        serialTelemetry3->begin(9600); 

        initTimer();
    }
}
