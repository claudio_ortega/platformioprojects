#include <Arduino.h>
#include "pins_arduino.h"  

#include "common/logger.hpp"
#include "i2c/Wire2.h"
#include "test/testListI2C.hpp"

namespace testListI2C {

    TwoWire2* w = nullptr;
    HardwareTimer* timer = nullptr;
    uint32_t testPin = 0;      

    uint64_t loopCount = 0;
    bool done = false;

    const int LAST_ADDRESS = 0x7F;
    const int FIRST_LOOP = 10;
    const int LAST_LOOP = FIRST_LOOP + LAST_ADDRESS;

    // keep the responsive addresses in an array, 
    // so we print them later to not to 
    // ruin the timing in the logic analyzer
    int responsive[0x80] = {0};
    int nextEmptyIndex = 0;

    void timerCallback() { 
        
        if ( done ) {
            return;
        }

        // avoid the first burst of timer ticks, Serial won't work!!
        loopCount++;

        if ( loopCount == FIRST_LOOP ) {
            logger::print (Serial, "scan -- BEGIN");
            digitalWrite(testPin, 1);
            w->begin(TwoWire2::MASTER_ADDRESS, false, 1000000);
            digitalWrite(testPin, 0);
        }

        else if ( loopCount > FIRST_LOOP && loopCount < (LAST_LOOP+2) ) {
            const int address = loopCount - FIRST_LOOP - 1;
            digitalWrite(testPin, 1);
            w->beginTransmission((uint8_t)address);
            const uint8_t error = w->endTransmission(1);
            digitalWrite(testPin, 0);
            if ( error == 0 ) {
                responsive[nextEmptyIndex] = address;
                nextEmptyIndex++;
            }
        }

        else if ( loopCount == (LAST_LOOP+2) ) {

            for ( int i=0; i<nextEmptyIndex; i++ ) {
                logger::print (Serial, "found I2C at: 0x%x", responsive[i]);
            }

            logger::print (Serial, "scan -- END");
            done = true;
        }
    }

    void init() {

        logger::print (Serial, "init() -- BEGIN");

        w = new TwoWire2(SDA, SCL);
        testPin = pinNametoDigitalPin(PA_5);
        pinMode(testPin, OUTPUT);

        timer = new HardwareTimer(TIM1);
        timer->setOverflow(500, MICROSEC_FORMAT);
        timer->attachInterrupt(timerCallback);
        timer->resume();

        logger::print (Serial, "init() -- END");
    }
}
