#include <Arduino.h>
#include "common/system2.hpp"
#include "test/testListI2C.hpp"
#include "test/testAll.hpp"
#include "common/logger.hpp"

void setup() {
    
    Serial.begin(115200);

    logger::print(Serial, "");
    logger::print(Serial, "setup() started");

    bool initStatus = true;

    switch ( 1 ) {
        case 0:
            testListI2C::init();
            break;
        case 1:
            initStatus = testAll::init(
                true,         // enable log
                true,         // enable pressure - DPS310
                true,         // enable imu - LSM6DS
                true,         // enable mag - LIS3MDL
                true,         // enable UART_RX
                true,         // enable UART_TX
                true,         // enable pwm
                1000000,      // I2C clock (hertz)
                921600,       // serial tx/rx baudrate (bauds)
                5000,         // main loop period (microsec)
                15000,        // compute size
                2,            // telemetry decimation factor
                1000);        // print decimation factor 
            break;
        default:
            initStatus = false;
            break;    
    }

    if ( initStatus ) {
        logger::print(Serial, "setup() succeeded");
    }
    else {
        logger::print(Serial, "setup() failed - halting");
        system2::halt();
    }
}

void loop() {
}
