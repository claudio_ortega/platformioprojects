#ifndef __pwm_hpp__
#define __pwm_hpp__

#include <Arduino.h>

class Pwm {
    public:
        void init(PinName pinName, int frequency);
        void setDutyCycle( float dutyCycle );
        void setDurationInMicroSec( uint32_t microsec );

    private:
        uint32_t channel = 0;
        HardwareTimer* hardwareTimer;
        int frequency;
        PinName pinName;
};


#endif
