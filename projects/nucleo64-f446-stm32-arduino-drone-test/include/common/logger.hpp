#ifndef __logger_hpp__
#define __logger_hpp__

#include <Arduino.h>
namespace logger {
    void print( HardwareSerial& serial, const char * format, ... );
}

#endif
