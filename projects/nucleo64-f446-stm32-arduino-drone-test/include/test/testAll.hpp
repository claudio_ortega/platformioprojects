#ifndef __testAll_hpp__
#define __testAll_hpp__

namespace testAll {
    bool init(
        bool inPrintOut,
        bool inUsePressure,
        bool inUseImu,
        bool inUseMag,
        bool inUseUART_RX,
        bool inUseUART_TX,
        bool inUsePwm,
        int clockI2C,
        int baudRate,
        int inPeriodMicroSec,
        int inComputeLoops,
        int inTelemetryLoops,
        int inPrintLoops );
}

#endif
