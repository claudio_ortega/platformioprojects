/*
 * This program is part of a bigger system designed to measure power
 * produced by a solar panel system.
 * 
 * We measure bi-phase line voltages L1/L2 coming from the inverter into the grid+house with
 * the corresponding current for each of L1/L2, and send the measurements over a serial line
 * with no buffering.
 * 
 * It samples 4 analog channels at a rate of 6K samples/sec with a resolution of 10 bits
 * in each sample, and sends the information using 5 bytes (4 x 10bits) per 4-channels-vector sample,
 * at 460800 bits/sec 8,N,1 over a serial asynchronous line,
 * for further processing by a separate processing unit.
 * 
 * It features a duty cycle of totalSeconds/producingSeconds (1:5),
 * starting with a period of producingSeconds seconds of sampling data, 
 * followed by totalSeconds-producingSeconds of silence.
 * Each of these cycles produces therefore a number of producingSeconds*samplingFrequencyInHz samples,
 * corresponding to producingSeconds*samplingFrequencyInHz*5 bytes
 * 
 * The processor on the receiving side of the serial will be responsible of
 * scaling, calibrating and computing power and cross power for Voltage/Current for both L1/L2 lines.
 * 
 * This module executes on an Atmel ATTiny841 with an external Xtal
 * of 14745600 Hz, chosen to produce the exactly required baud rate number.
 * 
 * This is hard real time system.
 * There is no buffering in this module, which means that it send information as fast as it produces it.
 * The delay introduced is of approximately 150 microSecs.
 */

#include <math.h>
#include <avr/interrupt.h>

typedef struct 
{
    // the real constants
    const unsigned long systemClockFreqInHz;  // should match the one on the X-tal in between PB0/PB1 pins
    const unsigned int  samplingFrequencyInHz;
    const unsigned long serialBaudRate;
    const unsigned int  producingSeconds;
    const unsigned int  totalSeconds;

    // derived constants at start time
    unsigned long producingCounts;
    unsigned long totalCounts;    
}
Constants;

static Constants constants = 
{
    // true constants
    .systemClockFreqInHz = 14745600,
    .samplingFrequencyInHz = 6000,
    .serialBaudRate = 460800,
    .producingSeconds = 1,
    .totalSeconds = 5,
    // these will be derived at start time
    .producingCounts = 0,
    .totalCounts = 0,
};

typedef struct 
{
    unsigned long sampleCount;  
} State;

void initDerivedConstants()
{
    // we add two to have one count for the begin of frame and another for the end
    constants.producingCounts = (long)constants.samplingFrequencyInHz * (long)constants.producingSeconds + 2;
    constants.totalCounts = (long)constants.samplingFrequencyInHz * (long)constants.totalSeconds;
}

void initDebugDerivedConstants()
{
    constants.producingCounts = 3 + 2;   // 3 samples, header, trailer = 5 total
    constants.totalCounts = (long)constants.samplingFrequencyInHz; // ie: repeat every second
}

void initSystemClock()
{
    // see section 6.6 in the ATTiny841 data sheet
    CCP = 0xD8;    // unlocks next command
    CLKPR = 0x00;  // scale = 1
    
    // wait the safety condition for setting the clock source
    while ( ! ( CLKCR & (1 << OSCRDY) ) ) 
    { 
    }
    
    CCP = 0xD8;    // unlocks next command
    CLKCR = 0x6F;  // use external X-tal oscillator, but do not output clock into PB1
}

void initUSARTForTx( unsigned long baudTxBitsSec )  // bits/sec
{
    // remap the TX0 output into pin PA7, away from the analog input pins
    REMAP = (1 << U0MAP);
    
    UCSR0B = (1 << TXEN0);   // enable tx only, see 19.8.2
    UCSR0C = 0b00000110;     // 8 bits, no parity, one stop bit, see 18.12.4

    const unsigned long baudRateDivisor = lround ( (float)constants.systemClockFreqInHz / ( 16.00 * (float) baudTxBitsSec ) ) - 1;
    UBRR0H = (baudRateDivisor >> 8) & 0xff;
    UBRR0L = baudRateDivisor & 0xff;
}

void initTimer1( int samplingFreqHz )
{  
    // Table 12-5. Waveform Generation Modes
    // 12.12.1 TCCR1A Timer/Counter Control Register A
    // 12.12.2 TCCR1B Timer/Counter Control Register B
    // set in here for timer 1 to CTC Mode --> 0100
    TCCR1A = 0;
    TCCR1A &= ~(1 << WGM10);
    TCCR1A &= ~(1 << WGM11);

    TCCR1B = 0;
    TCCR1B |=  (1 << WGM12);
    TCCR1B &= ~(1 << WGM13);
    
    // will compare the 2 bytes of timer1 counter against value in OCR1A
    OCR1A = ( (unsigned long) lround( ( (float)constants.systemClockFreqInHz / (float) samplingFreqHz ) ) ) - 1;
        
    // 12.12.2 TCCR1B Timer/Countern Control Register B
    // See table 12-6. Clock select bit description
    // set in here for 001 --> no pre-scaling
    TCCR1B &= ~ (1 << CS12);
    TCCR1B &= ~ (1 << CS11);
    TCCR1B |=   (1 << CS10);
            
    // 12.12.11 TIFR1  Timer/Counter Interrupt Flag Register
    // 12.12.10 TIMSK1 Timer/Counter Interrupt Mask Register
    // enable the overflow interrupt for Timer 1 when hitting OCR1A
    TIMSK1 = 0;
    TIMSK1 |= (1 << OCIE1A);
}

void initLEDPin()
{
    // make an output of PB2,
    // as it is the pin wired to the LED in the breakout board
    DDRB |= (1 << DDB2);
}

void initADC()
{
    // 16.13.2 ADMUXB -- ADC Multiplexer Selection Register
    // select the internal reference at 4.096v -> REFS[2:0] = 011
    ADMUXB = (1 << REFS1) | (1 << REFS0);
    
    // 16.13.4 ADCSRA -- ADC Control and Status Register A
    // disable automatic conversion and disable interruptions
    // we are going manual on ADC handling
    ADCSRA = 0;
    
    // we want ADC to be enabled
    ADCSRA |= (1 << ADEN);
    
    // set the ADC sar clock to something 0.5MHz-1.0Mhz
    // we need to use a 1:16 pre-scaler, as the system clock is around 14.75 MHz
    ADCSRA |= ( (1 << ADPS1) | (1 << ADPS0) );
    
    // select ADLAR=0,
    // the adc auto trigger is disabled as ADCSRA.ADATE=0
    ADCSRB = 0;

    // no intentions in saving power during AD conversion
    PRR &= ~(1<<PRADC);
}

void initUnusedPins()
{
    // set PA0, PA5, PA6 as outputs and ground them
    DDRA &= ~( (1 << DDA0) | (1 << DDA5) | (1 << DDA6) );
    PORTA |= ( (1 << PA0) | (1 << PA5) | (1 << PA6) );
}

void txOneByteOnUSART( unsigned char data )
{
    // wait for the tx USART to be ready to transmit
    while (!(UCSR0A & (1 << UDRE0)))
    { 
    }

    // transmit the next byte through the USART tx register
    UDR0 = data;
}

void doADConversionOnAllChannels( unsigned char data[], int nChannels )
{
    for ( int channel=0; channel<nChannels; channel++ )
    {
        // 16.13.1 ADMUXA -- ADC Multiplexer Selection Register A
        // - select the channel to sample next
        // channel->pin_name := 0->PA1, 1->PA2, 2->PA3, 3->PA4
        ADMUXA = channel+1;

        // 16.13.4 ADCSRA -- ADC Control and Status Register A
        // - start conversion
        ADCSRA |= (1 << ADSC);

        // 16.13.4 ADIF -- ADC Control and Status Register A
        // - wait until the conversion is done
        while ( (ADCSRA & (1 << ADIF) ) == 0 ) 
        { 
        }
        
        // this seems unnecessary, but the manual mentions
        // that you need to write a one on this bit to clear it,
        // see 16.13.4 Bit 4: ADIF
        ADCSRA |= (1 << ADIF);
                
        // conversion is ready, so then read all 10 bits
        // we are using ADLAR=0, 
        // see 6.13.3 ADCL and ADCH - ADC Data Register
        // also see comment in the manual on the need of
        // reading ADCL before ADCH
        data[2*channel]   = ADCL;         // 8 lsb bits ADC 7-0
        data[2*channel+1] = ADCH & 0x03;  // 2 msb bits ADC 9-8
     }
}

void compress8in5( unsigned char data8[], unsigned char data5[] )
{
    data5[0] = data8[0];
    data5[1] = data8[2];
    data5[2] = data8[4];
    data5[3] = data8[6];
    data5[4]  = ( data8[1] & 0x03 ) << 0;
    data5[4] |= ( data8[3] & 0x03 ) << 2;
    data5[4] |= ( data8[5] & 0x03 ) << 4;
    data5[4] |= ( data8[7] & 0x03 ) << 6;
}

void sampleAndTransmit()
{
    static unsigned char data8[8] = {0};   // the compiler does not like a variable for the size in here
    static unsigned char data5[5] = {0};

    doADConversionOnAllChannels(data8, 4);

    compress8in5(data8, data5);
    
    for ( int i=0; i<5; i++ ) 
    {
        txOneByteOnUSART( data5[i] );
    }
}

void transmitSamplingMarker( unsigned char marker )
{
    for ( int i=0; i<5; i++ ) 
    {
        txOneByteOnUSART( marker );
    }
}

void transmitBeginOfSamplingMarker()
{
    transmitSamplingMarker(0x55);
}

void transmitEndOfSamplingMarker()
{
    transmitSamplingMarker(0xaa);
}

void toggleLEDPin()
{
    static int ledOn = 0;

    ledOn = ( ledOn == 0 ) ? 1 : 0;

    if ( ledOn == 1 )
    {
        PORTB |= 1 << PB2;
    }
    else
    {
        PORTB &= ~( 1 << PB2 );
    }
}

ISR(TIMER1_COMPA_vect)
{
    // keep state in between calls
    static State samplingState = {0};

    toggleLEDPin();

    if ( samplingState.sampleCount == 0 )
    {
        transmitBeginOfSamplingMarker();
    }
    
    else if ( samplingState.sampleCount < ( constants.producingCounts - 1 ) )
    {
        sampleAndTransmit();
    }
    
    else if ( samplingState.sampleCount == ( constants.producingCounts - 1 ) ) 
    {
        transmitEndOfSamplingMarker();
    }
    
    else 
    {
        // should not transmit sample data up to the end of the current period
    }
    
    // keep the counting always in the closed interval [0, constants.totalCounts-1]
    samplingState.sampleCount = ( samplingState.sampleCount + 1 ) % constants.totalCounts;
}

int main(void)
{
    cli();
    
    initDerivedConstants();
    initSystemClock();
    initUSARTForTx( constants.serialBaudRate );
    initLEDPin();
    initTimer1( constants.samplingFrequencyInHz );
    initADC();
    initUnusedPins();

    sei();

    while(1)
    {
    }
}
