#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <IPAddress.h>

// Reference:
// https://github.com/nodemcu/nodemcu-devkit-v1.0

/*
    ESP8266 has two UARTS available UART0, UART1
    UART0 is bidireccional, but UART1 is only TX
    UART0 is the only one connected to USB over a CP2102.
    So, we have to give up the console functionality on the USB port connected to TXD0/RXD0,
    and connect UART1 to an external USB/UART FTDDI tranceiver going into the console in the computer at 115kb/s,
    and use UART0 to connect into tthe STM32 at 921kb/s
*/

// GPIO_2 is connected to a second blue led on the ESP12 piggyback board,
// GPIO_2 is also the TXD1 pin, so should not be used as a GPIO for this 
// application 

// GPIO_16 is conveniently connected to the blue led in the main board
const int BLUE_LED = 16;

// Serial1 is UART1 in the Arduino lib
HardwareSerial &serialDebug = Serial1;

// Serial0 is UART0 in the Arduino lib
HardwareSerial &telemetry = Serial;      

// 
const uint16_t rxBufferSize = 128;
uint8_t rxBuffer[rxBufferSize] = {0};

const char* ssid = "ortitos";
const char* password = "noelia2034";

void setup() {

    // set debug line standard baud rate, goes into dev computer  
    serialDebug.begin(115200);

    // this line will go to the STM32, then we want this line operating as fast as possible
    telemetry.begin(921600);
    telemetry.setRxBufferSize(1024);

    // blue LED is on board
    pinMode(BLUE_LED, OUTPUT);

    serialDebug.printf("\n\n\nprogram started\n");

    serialDebug.printf("attempting to connect to %s", ssid);

    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        serialDebug.print(".");
        delay(2000); 
    }
    serialDebug.println("\nconnected");
    serialDebug.printf("IP: %s\n", WiFi.localIP().toString().c_str());
    serialDebug.printf("SSID:%s, RSSI:%d\n", WiFi.SSID().c_str(), WiFi.RSSI());

    IPAddress serverAddress;
    serverAddress.fromString("192.168.1.98");
    serialDebug.printf("serverAddress:%s\n", serverAddress.toString().c_str());

    serialDebug.printf("tp: 1\n");
    delay(10000);
    serialDebug.printf("tp: 2\n");

    WiFiUDP* udp = new WiFiUDP();
    const uint8_t ok = udp->begin(4210);
    serialDebug.printf("ok:%d\n", ok);
}

void loop() {
    digitalWrite(BLUE_LED, LOW);  
    telemetry.write(0xaa);
    const size_t nFound = telemetry.read(rxBuffer, 1);
    if ( nFound > 0 ) {
        digitalWrite(BLUE_LED, HIGH);  
    }
}

