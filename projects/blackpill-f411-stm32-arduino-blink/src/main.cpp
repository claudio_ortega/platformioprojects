#include <Arduino.h>

const uint32_t BUILT_IN_LED = pinNametoDigitalPin(PC_13);
const uint32_t BUILT_IN_BUTTON = pinNametoDigitalPin(PA_0);
const uint32_t TEST_PIN = pinNametoDigitalPin(PA_5);
HardwareSerial* serial1 = nullptr;
HardwareSerial* serial2 = nullptr;
HardwareSerial* serial3 = nullptr;

long k1 = 0;
long k2 = 0;

void update_IT_callback() {
    
    static char msg[128] = {0};

    k1++;
    if ( ( k1 % 50000 ) == 0 ) {
        k2++;
        
        serial1->printf("hello(1):%d\n", k2);

        sprintf(msg, "hello(2):%d\n", k2);
        serial2->write(msg, strlen(msg));

        sprintf(msg, "hello(3):%d\n", k2);
        serial3->write(msg, strlen(msg));
    }

    if ( digitalRead( BUILT_IN_BUTTON ) == 0 || k2 % 2 == 0 ) {
        // turn off
        digitalWrite(BUILT_IN_LED, 1);
        digitalWrite(TEST_PIN, 1);
    }
    else {
        // toggle
        digitalWrite(BUILT_IN_LED, !digitalRead(BUILT_IN_LED));
        digitalWrite(TEST_PIN, !digitalRead(TEST_PIN));
    }
}

// called from main() we do not need/want to do anything in here,
// see ~/.platformio/packages/framework-arduinoststm32/cores/arduino/main.cpp
void initVariant() {}

// called from main() after returning from setup(),
// but as we are not returning from setup(), then we do not need to do anything in here
// as it is never reached,
// see ~/.platformio/packages/framework-arduinoststm32/cores/arduino/main.cpp
void loop() {}

// setup() is called from main(), and never returns,
// this app is totally interrupt driven
void setup() {

    serial1 = new HardwareSerial(PA_10, PA_9);
    serial2 = new HardwareSerial(PA_3,  PA_2);
    serial3 = new HardwareSerial(PA_12, PA_11);

    serial1->begin(115200); 
    serial2->begin(115200); 
    serial3->begin(115200); 

    pinMode(BUILT_IN_BUTTON, INPUT);
    pinMode(BUILT_IN_LED, OUTPUT);
    pinMode(TEST_PIN, OUTPUT);
    digitalWrite(BUILT_IN_LED, 0);
    digitalWrite(TEST_PIN, 0);

    HardwareTimer* const timer = new HardwareTimer(TIM1);
    timer->setOverflow(10, MICROSEC_FORMAT);
    timer->attachInterrupt(update_IT_callback);
    timer->resume();

    // we do not want or need to return, take a look at how main() works
    for (;;) {}
}


