#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <IPAddress.h>

// GPIO_16 is conveniently connected to the blue led in the main board
const int BLUE_LED = 16;

// Serial1 is UART1 in the Arduino lib
HardwareSerial &serialDebug = Serial;

void setup() {
    // set debug line standard baud rate, goes into dev computer
    serialDebug.begin(115200);
    // blue LED is on board
    pinMode(BLUE_LED, OUTPUT);
    serialDebug.printf("\n\n\nprogram started\n");
}

void loop() {
    digitalWrite(BLUE_LED, LOW);  
    delay(100);
    digitalWrite(BLUE_LED, HIGH);
    delay(100);
}

