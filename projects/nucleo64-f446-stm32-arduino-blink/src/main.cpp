#include <Arduino.h>
#include <variant_NUCLEO_F446RE.h>  // to make explicit the PIN defs source

const uint32_t GREEN_LED = LED_BUILTIN;   // this is PA5
const uint32_t BLUE_BUTTON = PC13;

void Update_IT_callback() { 
    if ( digitalRead( BLUE_BUTTON ) == 1 ) {
        digitalWrite(GREEN_LED, !digitalRead(GREEN_LED));
    }
    else {
        digitalWrite(GREEN_LED, 0);
    }
}

void setup() {
    pinMode(BLUE_BUTTON, INPUT);
    pinMode(GREEN_LED, OUTPUT);
    HardwareTimer* const timer = new HardwareTimer(TIM1);
    timer->attachInterrupt(Update_IT_callback);
    timer->setOverflow(2000, HERTZ_FORMAT); 
    timer->resume();
}

void loop() {
}
