#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <errno.h>
#include <cstdlib>
#include "common/telemetry.hpp"

namespace telemetry {

    static uint8_t header[6] = {0x55,0x55,0x55,0x55,0x55,0x55};
    static uint8_t payload_length_bytes[2] = {0x00,0x00};
    static uint8_t crc[4] = {0x55,0x55,0x55,0x55};
    static uint8_t payload[1024] = {0};

    void print( 
        HardwareSerial* serial, 
        Arduino_CRC32* crc32, 
        const uint64_t time_index_64, 
        const float* in, 
        const uint16_t len ) {

        const uint32_t time_index_32 = time_index_64 & 0xffffffff;

        payload[0] = 0xff & (time_index_32);
        payload[1] = 0xff & (time_index_32 >> 8);
        payload[2] = 0xff & (time_index_32 >> 16);
        payload[3] = 0xff & (time_index_32 >> 24);
        
        memcpy( payload + sizeof(uint32_t), in, len * sizeof(float));

        const int payload_length = sizeof(uint32_t) + len * sizeof(float);
        payload_length_bytes[0] = 0xff & (payload_length);
        payload_length_bytes[1] = 0xff & (payload_length >> 8);

        const uint32_t c = crc32->calc(payload, payload_length);
        crc[0] = 0xff & (c);
        crc[1] = 0xff & (c >> 8);
        crc[2] = 0xff & (c >> 16);
        crc[3] = 0xff & (c >> 24);

        serial->write( (uint8_t*)header, sizeof(header) );
        serial->write( (uint8_t*)payload_length_bytes, sizeof(payload_length_bytes) );
        serial->write( (uint8_t*)payload, payload_length );
        serial->write( (uint8_t*)crc, sizeof(crc) );
    }
}


