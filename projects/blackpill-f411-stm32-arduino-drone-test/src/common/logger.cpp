#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <errno.h>
#include <cstdlib>
#include "common/logger.hpp"

namespace logger {

    static void shiftRight( char* buffer, int bufferlen, int shift ) {
        int to = bufferlen-1;
        int from = to - shift;
        while (from >= 0 ) {
            buffer[to]=buffer[from];
            to--;
            from = to - shift;
        }
        for ( int i=0; i<shift; i++ ) {
            buffer[i] = 0;
        }
    }
   
    /*
        in:       out:
        0         0.000
        1         0.001
        12        0.012
        123       0.123
        1234      1.234
        12345    12.345
    */
    static bool insertDotIfPositive(
        char* buffer, 
        int bufferlen,
        int fractlen ) {

        const int len = strlen(buffer);

        if ( len <= fractlen ) {
            const int offset = fractlen - len + 2;  //  0.  has length 2
            shiftRight( buffer, bufferlen, offset );
            for ( int i=0; i<offset; i++ ) {
                buffer[i]='0';
            }
            buffer[1] = '.';
        }
        else {
            const int dotPosition = len - fractlen;
            shiftRight( buffer+dotPosition, bufferlen, 1 );
            buffer[dotPosition] = '.';            
        }

        return true;
    } 

    static bool insertDot(
        char* buffer, 
        int bufferlen,
        int fractlen ) {
        if ( buffer[0] == '-' ) {
            return insertDotIfPositive( buffer+1, bufferlen-1, fractlen );
        }   
        else {
            return insertDotIfPositive( buffer, bufferlen, fractlen );
        } 
    }

    static bool printBuffer( char buffer[], int maxChars, const char* fmt, va_list args ) {

        memset(buffer, 0, maxChars);
    
        int ix = 0;
        static char tmp[32] = {0};

        for ( int i=0; i<strlen(fmt); ) {

            if (fmt[i] == '%' && fmt[i+1] == 'd' ) {
                const int x = va_arg(args, int);
                memset(tmp, 0, sizeof(tmp));
                itoa(x, tmp, 10);
                strncpy( buffer+ix, tmp, strlen(tmp));
                ix = ix + strlen(tmp);
                i = i + 2;
            }
            else if (fmt[i] == '%' && fmt[i+1] == 'x' ) {
                const int x = va_arg(args, int);
                memset(tmp, 0, sizeof(tmp));
                itoa(x, tmp, 16);
                strncpy( buffer+ix, tmp, strlen(tmp));
                ix = ix + strlen(tmp);
                i = i + 2;
            }
            // floats get promoted to double when passed on ...
            else if (fmt[i] == '%' && fmt[i+1] == 'f' ) {
                const double d = va_arg(args, double);
                const long m = (long) ( d * 1000.0 );
                memset(tmp, 0, sizeof(tmp));
                ltoa(m, tmp, 10);
                if ( !insertDot( tmp, sizeof(tmp), 3 ) ) {
                    return false;
                }
                strncpy( buffer+ix, tmp, strlen(tmp));
                ix = ix + strlen(tmp);
                i = i + 2;
            }
            else {
                buffer[ix] = fmt[i];
                ix++;
                i++;
            }

            if ( ix > maxChars ) {
                return false;
            }
        }
    
        va_end( args );

        return true;
    }

    static bool snprintf( char buffer[], int maxChars, const char* fmt, ... ) {
        va_list args;
        va_start( args, fmt );
        return printBuffer( buffer, maxChars, fmt, args );
    }

    void print( HardwareSerial* serialIn, const char* fmt, ... ) {
        
        static char buffer[256] = {0};
        static uint64_t line_count = 0;

        va_list args;
        va_start( args, fmt );

        if ( printBuffer( buffer, sizeof(buffer), fmt, args ) ) {
            serialIn->printf( "%06d ", line_count++ ); 
            serialIn->println( buffer ); 
            serialIn->flush();
        }
        else {
            serialIn->println( "error in logger::print" ); 
            while(1) {}
        }
    }
}


