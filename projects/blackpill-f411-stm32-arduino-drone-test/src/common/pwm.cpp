#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <errno.h>
#include <cstdlib>
#include "common/logger.hpp"
#include "common/pwm.hpp"

void Pwm::init(PinName inPinName, int inFrequency) {
    
    pinName = inPinName;
    frequency = inFrequency;
    pinMode(pinNametoDigitalPin(pinName), OUTPUT);
    
    TIM_TypeDef *timer = (TIM_TypeDef *)pinmap_peripheral(pinName, PinMap_PWM);
    channel = STM_PIN_CHANNEL(pinmap_function(pinName, PinMap_PWM));

    hardwareTimer = new HardwareTimer(timer);
    hardwareTimer->setPWM(channel, pinNametoDigitalPin(pinName), frequency, 0); 
}

void Pwm::setDutyCycle( float dutyCycle ) {
    hardwareTimer->setCaptureCompare(channel, 100.0*dutyCycle, PERCENT_COMPARE_FORMAT);
}

void Pwm::setDurationInMicroSec( uint32_t microsec ) {
    hardwareTimer->setCaptureCompare(channel, microsec, MICROSEC_COMPARE_FORMAT);
}
