#ifndef ADAFRUIT_LIS3MDL_H
#define ADAFRUIT_LIS3MDL_H

#include "i2c/Adafruit_BusIO_Register.h"
#include "i2c/Adafruit_I2CDevice.h"
#include "i2c/Wire2.h"

typedef enum {
  LIS3MDL_RANGE_4_GAUSS = 0b00,  ///< +/- 4g (default value)
  LIS3MDL_RANGE_8_GAUSS = 0b01,  ///< +/- 8g
  LIS3MDL_RANGE_12_GAUSS = 0b10, ///< +/- 12g
  LIS3MDL_RANGE_16_GAUSS = 0b11, ///< +/- 16g
} lis3mdl_range_t;
typedef enum {
  LIS3MDL_DATARATE_0_625_HZ = 0b0000, ///<  0.625 Hz
  LIS3MDL_DATARATE_1_25_HZ = 0b0010,  ///<  1.25 Hz
  LIS3MDL_DATARATE_2_5_HZ = 0b0100,   ///<  2.5 Hz
  LIS3MDL_DATARATE_5_HZ = 0b0110,     ///<  5 Hz
  LIS3MDL_DATARATE_10_HZ = 0b1000,    ///<  10 Hz
  LIS3MDL_DATARATE_20_HZ = 0b1010,    ///<  20 Hz
  LIS3MDL_DATARATE_40_HZ = 0b1100,    ///<  40 Hz
  LIS3MDL_DATARATE_80_HZ = 0b1110,    ///<  80 Hz
  LIS3MDL_DATARATE_155_HZ = 0b0001,   ///<  155 Hz (FAST_ODR + UHP)
  LIS3MDL_DATARATE_300_HZ = 0b0011,   ///<  300 Hz (FAST_ODR + HP)
  LIS3MDL_DATARATE_560_HZ = 0b0101,   ///<  560 Hz (FAST_ODR + MP)
  LIS3MDL_DATARATE_1000_HZ = 0b0111,  ///<  1000 Hz (FAST_ODR + LP)
} lis3mdl_dataRate_t;

/** The magnetometer performance mode */
typedef enum {
  LIS3MDL_LOWPOWERMODE = 0b00,  ///< Low power mode
  LIS3MDL_MEDIUMMODE = 0b01,    ///< Medium performance mode
  LIS3MDL_HIGHMODE = 0b10,      ///< High performance mode
  LIS3MDL_ULTRAHIGHMODE = 0b11, ///< Ultra-high performance mode
} lis3mdl_performancemode_t;

/** The magnetometer operation mode */
typedef enum {
  LIS3MDL_CONTINUOUSMODE = 0b00, ///< Continuous conversion
  LIS3MDL_SINGLEMODE = 0b01,     ///< Single-shot conversion
  LIS3MDL_POWERDOWNMODE = 0b11,  ///< Powered-down mode
} lis3mdl_operationmode_t;

typedef struct {
    float x;
    float y;
    float z;
} xyz_mag_triplet;

/** Class for hardware interfacing with an LIS3MDL magnetometer */
class Adafruit_LIS3MDL {
    public:
        static const uint8_t LIS3MDL_I2CADDR_PRIMARY = 0x1C;
        static const uint8_t LIS3MDL_I2CADDR_SECONDARY = 0x1E;
        
        Adafruit_LIS3MDL();
        virtual ~Adafruit_LIS3MDL();
        
        bool begin_I2C(uint8_t i2c_addr, TwoWire2 *wire);
        bool reset();
        bool init();
        
        bool pollRegisters();
        bool setPerformanceMode(lis3mdl_performancemode_t mode);
        bool setOperationMode(lis3mdl_operationmode_t mode);
        bool setDataRate(lis3mdl_dataRate_t dataRate);
        bool setRange(lis3mdl_range_t range);
        bool setIntThreshold(uint16_t value);

        bool configInterrupt(
            bool enableX, 
            bool enableY, 
            bool enableZ, 
            bool polarity,
            bool latch, 
            bool enableInt);
        bool selfTest(bool flag);

        bool magneticFieldAvailable();
        void getMag( xyz_mag_triplet* mag );

    private:
        Adafruit_I2CDevice *i2c_dev;

        bool pollRawRegisters();
        // last read mag in raw units
        int16_t x_raw;     
        int16_t y_raw;
        int16_t z_raw;

        void scaleRawRegisters();
        // last read mag in gauss
        float x_mgauss;
        float y_mgauss;
        float z_mgauss;

        lis3mdl_range_t range;

        float scale;
};

#endif

