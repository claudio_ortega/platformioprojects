#include "Arduino.h"
#include "sensors/Adafruit_LIS3MDL.h"


typedef struct {
    uint8_t LIS3MDL_REG_WHO_AM_I = 0x0F;     ///< Register that contains the part ID
    uint8_t LIS3MDL_REG_CTRL_REG1 = 0x20;    ///< Register address for control 1
    uint8_t LIS3MDL_REG_CTRL_REG2 = 0x21;    ///< Register address for control 2
    uint8_t LIS3MDL_REG_CTRL_REG3 = 0x22;    ///< Register address for control 3
    uint8_t LIS3MDL_REG_CTRL_REG4 = 0x23;    ///< Register address for control 3
    uint8_t LIS3MDL_REG_STATUS = 0x27;       ///< Register address for status
    uint8_t LIS3MDL_REG_OUT_X_L = 0x28;      ///< Register address for X axis lower byte
    uint8_t LIS3MDL_REG_INT_CFG = 0x30;      ///< Interrupt configuration register
    uint8_t LIS3MDL_REG_INT_THS_L = 0x32;    ///< Low byte of the irq threshold
} LIS3MDL_registers_t;

static const LIS3MDL_registers_t registers;

Adafruit_LIS3MDL::Adafruit_LIS3MDL() {
    range = LIS3MDL_RANGE_4_GAUSS;
    i2c_dev = NULL;
    x_raw = 0;
    y_raw = 0;
    z_raw = 0;
    x_mgauss = 0;
    y_mgauss = 0;
    z_mgauss = 0;
    scale = 1.0;
}

Adafruit_LIS3MDL::~Adafruit_LIS3MDL() {}

/*!
 *    @brief  Sets up the hardware and initializes I2C
 *    @param  i2c_address
 *            The I2C address to be used.
 *    @param  wire
 *            The Wire object to be used for I2C connections.
 *    @return True if initialization was successful, otherwise false.
 */
bool Adafruit_LIS3MDL::begin_I2C(
    uint8_t i2c_address, 
    TwoWire2 *wire) {

    if ( i2c_dev != nullptr ) {
        return false;
    }

    i2c_dev = new Adafruit_I2CDevice(i2c_address, wire);

    return true;
}

bool Adafruit_LIS3MDL::reset() {

    Adafruit_BusIO_Register ctrl2 = Adafruit_BusIO_Register(
        i2c_dev, 
        registers.LIS3MDL_REG_CTRL_REG2, 
        1,
        LSBFIRST,
        1);

    Adafruit_BusIO_RegisterBits sw_reset = Adafruit_BusIO_RegisterBits(
        &ctrl2, 
        1, 
        2);
        
    if ( ! sw_reset.write(0x01) ) {
        return false;
    }
    
    uint32_t val = 0;
    while ( val != 0 ) {
        bool test = sw_reset.read(&val);
        if ( !test ) {
            return false;
        }
    }

    return true;
}

bool Adafruit_LIS3MDL::init() {
    if ( !reset() ) {
        return false;
    }
    
    if ( !setOperationMode(LIS3MDL_CONTINUOUSMODE) ) {
        return false;
    }

    if ( !setDataRate(LIS3MDL_DATARATE_300_HZ) ) {
        return false;
    }

    if ( !setRange(LIS3MDL_RANGE_8_GAUSS) ) {
        return false;
    }

    return true;
}

bool Adafruit_LIS3MDL::pollRegisters() {
    
    const bool status = pollRawRegisters();
    if ( !status ) {
        return false;
    }

    scaleRawRegisters();

    return true;
}

bool Adafruit_LIS3MDL::pollRawRegisters() {

    static uint8_t buffer[6] = {0};
    memset(buffer, 0, sizeof(buffer) );

    const bool status = Adafruit_BusIO_Register(
        i2c_dev, 
        registers.LIS3MDL_REG_OUT_X_L, 
        sizeof(buffer),
        LSBFIRST,
        1).read(buffer, sizeof(buffer) );

    if ( !status ) {
        return false;
    }

    x_raw = buffer[0] | (buffer[1] << 8);
    y_raw = buffer[2] | (buffer[3] << 8);
    z_raw = buffer[4] | (buffer[5] << 8);

    return true;
}

void Adafruit_LIS3MDL::scaleRawRegisters() {
    x_mgauss = (float)x_raw * scale * 1000;
    y_mgauss = (float)y_raw * scale * 1000;
    z_mgauss = (float)z_raw * scale * 1000;
}

void Adafruit_LIS3MDL::getMag( xyz_mag_triplet* mag ){
    mag->x = x_mgauss;
    mag->y = y_mgauss;
    mag->z = z_mgauss;
}

bool Adafruit_LIS3MDL::setPerformanceMode(lis3mdl_performancemode_t mode) {

    // write xy
    Adafruit_BusIO_Register CTRL_REG1 = Adafruit_BusIO_Register(
        i2c_dev, 
        registers.LIS3MDL_REG_CTRL_REG1, 
        1,
        LSBFIRST,
        1);

    if ( !Adafruit_BusIO_RegisterBits(
        &CTRL_REG1, 
        2, 
        5).write((uint8_t)mode) ) {
        return false;
    }

    // write z
    Adafruit_BusIO_Register CTRL_REG4 = Adafruit_BusIO_Register(
        i2c_dev, 
        registers.LIS3MDL_REG_CTRL_REG4, 
        1,
        LSBFIRST,
        1);
    
    if ( !Adafruit_BusIO_RegisterBits(
        &CTRL_REG4, 
        2, 
        2).write((uint8_t)mode) ) {
        return false;
    }

    return true;    
}

/*!
    @brief  Sets the data rate for the LIS3MDL (controls power consumption)
    from 0.625 Hz to 80Hz
    @param dataRate Enumerated lis3mdl_dataRate_t
*/
bool Adafruit_LIS3MDL::setDataRate(lis3mdl_dataRate_t dataRate) {

    bool status = false;

    if (dataRate == LIS3MDL_DATARATE_155_HZ) {
        // set OP to UHP
        status = setPerformanceMode(LIS3MDL_ULTRAHIGHMODE);
    }
    if (dataRate == LIS3MDL_DATARATE_300_HZ) {
        // set OP to HP
        status = setPerformanceMode(LIS3MDL_HIGHMODE);
    }
    if (dataRate == LIS3MDL_DATARATE_560_HZ) {
        // set OP to MP
        status = setPerformanceMode(LIS3MDL_MEDIUMMODE);
    }
    if (dataRate == LIS3MDL_DATARATE_1000_HZ) {
        // set OP to LP
        status = setPerformanceMode(LIS3MDL_LOWPOWERMODE);
    }

    if ( !status) {
        return false;
    }

    Adafruit_BusIO_Register CTRL_REG1 = Adafruit_BusIO_Register(
        i2c_dev, 
        registers.LIS3MDL_REG_CTRL_REG1, 
        1,
        LSBFIRST,
        1);

    return Adafruit_BusIO_RegisterBits(
        &CTRL_REG1, 
        4, 
        1).write((uint8_t)dataRate);
}

bool Adafruit_LIS3MDL::setOperationMode(lis3mdl_operationmode_t mode) {
    Adafruit_BusIO_Register CTRL_REG3 = Adafruit_BusIO_Register(
        i2c_dev, 
        registers.LIS3MDL_REG_CTRL_REG3, 
        1,
        LSBFIRST,
        1);

    return Adafruit_BusIO_RegisterBits(
        &CTRL_REG3, 
        2, 
        0).write((uint8_t)mode);
}

bool Adafruit_LIS3MDL::setRange(lis3mdl_range_t range) {
    scale = 1.0; // LSB per gauss
    if (range == LIS3MDL_RANGE_16_GAUSS)  scale = 1.0 / 1711.0;
    if (range == LIS3MDL_RANGE_12_GAUSS)  scale = 1.0 / 2281.0;
    if (range == LIS3MDL_RANGE_8_GAUSS)   scale = 1.0 / 3421.0;
    if (range == LIS3MDL_RANGE_4_GAUSS)   scale = 1.0 / 6842.0;

    Adafruit_BusIO_Register CTRL_REG2 = Adafruit_BusIO_Register(
        i2c_dev, 
        registers.LIS3MDL_REG_CTRL_REG2, 
        1,
        LSBFIRST,
        1);
    
    return Adafruit_BusIO_RegisterBits(&CTRL_REG2, 2, 5).write((uint8_t)range);
}

bool Adafruit_LIS3MDL::setIntThreshold(uint16_t value) {
    return Adafruit_BusIO_Register(
        i2c_dev, 
        registers.LIS3MDL_REG_INT_THS_L, 
        2,
        LSBFIRST,
        1).write(value & 0x7FFF, 0);
}

/*!
    @brief Configure INT_CFG
    @param enableX Enable interrupt generation on X-axis
    @param enableY Enable interrupt generation on Y-axis
    @param enableZ Enable interrupt generation on Z-axis
    @param polarity Sets the polarity of the INT output logic
    @param latch If true (latched) the INT pin remains in the same state
    until INT_SRC is read.
    @param enableInt Interrupt enable on INT pin
*/
bool Adafruit_LIS3MDL::configInterrupt(
    bool enableX,
    bool enableY,
    bool enableZ,
    bool polarity,
    bool latch,
    bool enableInt) {
                                           
    uint8_t value = 0x08; // set default bits, see table 36
    value |= enableX << 7;
    value |= enableY << 6;
    value |= enableZ << 5;
    value |= polarity << 2;
    value |= latch << 1;
    value |= enableInt;
        
    return Adafruit_BusIO_Register(
        i2c_dev, 
        registers.LIS3MDL_REG_INT_CFG, 
        1,
        LSBFIRST,
        1).write(value, 0);
}

/*!
    @brief Enable or disable self-test
    @param flag If true, enable self-test
*/
bool Adafruit_LIS3MDL::selfTest(bool flag) {
    Adafruit_BusIO_Register CTRL_REG1 = Adafruit_BusIO_Register(
        i2c_dev, 
        registers.LIS3MDL_REG_CTRL_REG1, 
        1,
        LSBFIRST,
        1);

    return Adafruit_BusIO_RegisterBits(&CTRL_REG1, 1, 0).write(flag);
}


bool Adafruit_LIS3MDL::magneticFieldAvailable() {   
    uint32_t val = 0; 
    Adafruit_BusIO_Register(
        i2c_dev, 
        registers.LIS3MDL_REG_STATUS, 
        1,
        LSBFIRST,
        1).read(&val);

    return (val & 0x08 == 0) ? false : true;
}

