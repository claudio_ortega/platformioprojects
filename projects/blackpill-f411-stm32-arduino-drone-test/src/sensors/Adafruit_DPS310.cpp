/**************************************************************************/
/**
 *  @file     Adafruit_DPS310.cpp
 *  @author   Limor Fried (Adafruit Industries) / Claudio Ortega (Pera Labs)
 *  @mainpage Adafruit DSP310 Barometric Pressure Sensor Library
 *  @section intro_sec Introduction
 *
 * 	I2C Driver for the [Adafruit DPS310 barometric pressure breakout board]
 *    (https://www.adafruit.com/product/4494)
 *
 *  Adafruit invests time and resources providing this open source code,
 *    please support Adafruit and open-source hardware by purchasing products from
 * 	  Adafruit!
 *  @see Adafruit_DPS310

 *  @section dependencies Dependencies
 *  This library depends on the [Adafruit BusIO library]
 *   (https://github.com/adafruit/Adafruit_BusIO) and the 
 *   [Adafruit Unified Sensor Library]
 *   (https://github.com/adafruit/Adafruit_Sensor)

 * 	@section license BSD (see license.txt)
*/
/**************************************************************************/

#include <Arduino.h>
#include "sensors/Adafruit_DPS310.h"
#include "common/logger.hpp"

typedef enum {
    DPS310_MEASCFG = 0x08,       // Sensor configuration
    DPS310_PRSB2 = 0x00,         // MSB pressure data
    DPS310_TMPB2 = 0x03,         // MSB temperature data
    DPS310_PRSCFG = 0x06,        // Pressure configuration
    DPS310_TMPCFG = 0x07,        // Temperature configuration
    DPS310_CFGREG = 0x09,        // Interrupt/FIFO configuration
    DPS310_RESET = 0x0C,         // Soft reset
    DPS310_PRODREVID = 0x0D,     // Register that contains the part ID
    DPS310_TMPCOEFSRCE = 0x28,   // Temperature calibration src
} register_310_t;

static float oversample_scalefactor[] = {
     524288.0f, 
    1572864.0f, 
    3670016.0f, 
    7864320.0f,
     253952.0f, 
     516096.0f,  
    1040384.0f, 
    2088960.0f,
};

Adafruit_DPS310::Adafruit_DPS310() {
    i2c_dev = nullptr;
    _raw_pressure = 0;
    _pressure = 0.0f;
    _raw_temperature = 0;
    _temperature = 0.0f;
    _temperature_scale = 0.0f;
    _pressure_scale = 0.0f;
}

Adafruit_DPS310::~Adafruit_DPS310() {}

/*
 *    @brief  Sets up the hardware and initializes I2C
 *    @param  i2c_address
 *            The I2C address to be used.
 *    @param  wire
 *            The Wire object to be used for I2C connections.
 *    @return True if initialization was successful, otherwise false.
 */
bool Adafruit_DPS310::begin_I2C(
    uint8_t i2c_address, 
    TwoWire2 *wire) {

    if (i2c_dev != nullptr) {
        return false;
    }

    i2c_dev = new Adafruit_I2CDevice(i2c_address, wire);
    
    return true;
}

bool Adafruit_DPS310::init() {

    if ( !reset() ) {
        return false;
    }

    while (!sensorAvailable()) {
    }

    while (!calibrationAvailable()) {
    }

    if ( !readCalibration() ) {
        return false;
    }    

    if ( !configurePressure(DPS310_128HZ, DPS310_1SAMPLE) ) {
        return false;
    }

    if ( ! configureTemperature(DPS310_128HZ, DPS310_1SAMPLE) ) {
        return false;
    }
    
    if ( ! setMode(DPS310_CONT_PRESTEMP) ) {
        return false;
    }

    while (!temperatureAvailable() || !pressureAvailable()) {
    }

    return true;
}

/*
    @brief Resets the sensor to its power-on state, clearing all registers and memory
*/
bool Adafruit_DPS310::reset() {

    Adafruit_BusIO_Register reset = Adafruit_BusIO_Register(
        i2c_dev, 
        DPS310_RESET,
        1,
        LSBFIRST,
        1);

    Adafruit_BusIO_RegisterBits reset_bit = Adafruit_BusIO_RegisterBits(
        &reset, 
        1, 
        0);

    if ( ! reset_bit.write(0x01) ) {
        return false;
    }

    uint32_t val = 0;
    while ( val != 0 ) {
        bool test = reset_bit.read(&val);
        if ( !test ) {
            return false;
        }
    }

    return true;
}

static int32_t twosComplement(int32_t val, uint8_t bits) {
    if (val & ((uint32_t)1 << (bits - 1))) {
        val -= (uint32_t)1 << bits;
    }
    return val;
}

bool Adafruit_DPS310::readCalibration() {

    uint8_t coeffs[18] = {0};
    for (uint8_t i=0; i<18; i++) {
        Adafruit_BusIO_Register coeff = Adafruit_BusIO_Register(
            i2c_dev, 
            0x10 + i, 
            1,
            LSBFIRST,
            1);
        uint32_t val = 0;
        if ( ! coeff.read(&val) ) {
            return false;
        }
        coeffs[i] = (uint8_t) val;
    }

    c0  = ( ((uint16_t)coeffs[0]) << 4) | ((((uint16_t)coeffs[1]) >> 4) & 0x0F);
    c0  = twosComplement(c0, 12);

    c1  = (((uint16_t)coeffs[1] & 0x0F) << 8) | coeffs[2], 12;
    c1  = twosComplement(c1, 12);

    c00 = ((uint32_t)coeffs[3] << 12) | ((uint32_t)coeffs[4] << 4) | (((uint32_t)coeffs[5] >> 4) & 0x0F);
    c00 = twosComplement(c00, 20);

    c10 = (((uint32_t)coeffs[5] & 0x0F) << 16) | ((uint32_t)coeffs[6] << 8) | ((uint32_t)coeffs[7]);
    c10 = twosComplement(c10, 20);

    c01 = ((uint16_t)coeffs[8] << 8)  | ((uint16_t)coeffs[9]);
    c01 = twosComplement(c01, 16);

    c11 = ((uint16_t)coeffs[10] << 8) | ((uint16_t)coeffs[11]);
    c11 = twosComplement(c11, 16);

    c20 = ((uint16_t)coeffs[12] << 8) | ((uint16_t)coeffs[13]);
    c20 = twosComplement(c20, 16);

    c21 = ((uint16_t)coeffs[14] << 8) | ((uint16_t)coeffs[15]);
    c21 = twosComplement(c21, 16);

    c30 = ((uint16_t)coeffs[16] << 8) | ((uint16_t)coeffs[17]);
    c30 = twosComplement(c30, 16);

    return true;
}

bool Adafruit_DPS310::readAndLogRegisters(HardwareSerial* serialDebug) {
    static const int N_REGS = 0x29;
    static uint8_t registers[N_REGS] = {0};
    uint32_t val = 0;
    for (uint8_t i=0; i<N_REGS; i++) {
        Adafruit_BusIO_Register r = Adafruit_BusIO_Register(
            i2c_dev, 
            i, 
            1,
            LSBFIRST,
            1);
        if ( ! r.read(&val) ) {
            return false;
        }
        registers[i] = (uint8_t) val;
        logger::print( serialDebug, "registers[%x]=0x%x", i, registers[i] );
    }
    return true;
}

/**
 *  in:
 *  registerId:  as in Register map table in datasheet
 *  bitposition: 0:LSB, 7:MSB      
 *  out:         pointer to result, either 0x00, or 0x01 
 * 
 *  return:
 *  true if succesful
 * 
 */
bool Adafruit_DPS310::readBitFromRegister(uint8_t registerId, uint8_t bitposition, uint8_t *out) {

    Adafruit_BusIO_Register MEAS_CFG = Adafruit_BusIO_Register(
        i2c_dev, 
        registerId, 
        1,
        LSBFIRST,
        1);

    uint32_t val = 0;

    if ( !Adafruit_BusIO_RegisterBits(
        &MEAS_CFG, 
        1, 
        bitposition).read(&val) ) {
            return false;
        }

    *out = val;

    return val != 0;
}

bool Adafruit_DPS310::temperatureAvailable() {
    uint8_t val = 0;
    if ( ! readBitFromRegister(DPS310_MEASCFG, 5, &val) ) {
        return false;
    }
    return val == 0x01;
}

bool Adafruit_DPS310::pressureAvailable() {
    uint8_t val = 0;
    if ( ! readBitFromRegister(DPS310_MEASCFG, 4, &val) ) {
        return false;
    }
    return val == 0x01;
}

bool Adafruit_DPS310::sensorAvailable() {
    uint8_t val = 0;
    if ( ! readBitFromRegister(DPS310_MEASCFG, 6, &val) ) {
        return false;
    }
    return val == 0x01;
}

// see datasheet, MEAS_CFG, bit 7
bool Adafruit_DPS310::calibrationAvailable() {
    uint8_t val = 0;
    if ( ! readBitFromRegister(DPS310_MEASCFG, 7, &val) ) {
        return false;
    }
    return val == 0x01;
}

/*
    @brief  Set the operational mode of the sensor (continuous or one-shot)
    @param mode can be DPS310_IDLE, one shot: DPS310_ONE_PRESSURE or
   DPS310_ONE_TEMPERATURE, continuous: DPS310_CONT_PRESSURE, DPS310_CONT_TEMP,
   DPS310_CONT_PRESTEMP
*/
bool Adafruit_DPS310::setMode(dps310_mode_t mode) {

    Adafruit_BusIO_Register MEAS_CFG = Adafruit_BusIO_Register(
        i2c_dev, 
        DPS310_MEASCFG, 
        1,
        LSBFIRST,
        1);
    
    return Adafruit_BusIO_RegisterBits(
        &MEAS_CFG, 
        3, 
        0).write(mode);
}

/*
    @brief Set the sample rate and oversampling averaging for pressure
    @param rate How many samples per second to take
    @param os How many oversamples to average
*/
bool Adafruit_DPS310::configurePressure(
    dps310_rate_t rate,
    dps310_oversample_t os) {

    Adafruit_BusIO_Register PRS_CFG = Adafruit_BusIO_Register(
        i2c_dev, 
        DPS310_PRSCFG, 
        1,
        LSBFIRST,
        1);

    if ( !Adafruit_BusIO_RegisterBits(&PRS_CFG, 3, 4).write(rate) ) {
        return false;
    }
    
    if ( !Adafruit_BusIO_RegisterBits(&PRS_CFG, 4, 0).write(os) ) {
        return false;
    }

    _pressure_scale = oversample_scalefactor[os];

    // Set shift bit if necessary
    Adafruit_BusIO_Register CFG_REG = Adafruit_BusIO_Register(
        i2c_dev, 
        DPS310_CFGREG, 
        1,
        LSBFIRST,
        1);

    if ( !Adafruit_BusIO_RegisterBits(
        &CFG_REG, 
        1, 
        2).write( (os > DPS310_8SAMPLES) ? 0x01 : 0x00 ) ) {
            return false;
    }

    return true;
}

/*
    @brief Set the sample rate and oversampling averaging for temperature
    @param rate How many samples per second to take
    @param os How many oversamples to average
*/
bool Adafruit_DPS310::configureTemperature(
    dps310_rate_t rate,
    dps310_oversample_t os) {

    Adafruit_BusIO_Register TMP_CFG = Adafruit_BusIO_Register(
        i2c_dev, 
        DPS310_TMPCFG, 
        1,
        LSBFIRST,
        1);

    if ( !Adafruit_BusIO_RegisterBits(&TMP_CFG, 3, 4).write(rate) ) {
        return false;
    }
    
    if ( !Adafruit_BusIO_RegisterBits(&TMP_CFG, 4, 0).write(os) ) {
        return false;
    }

    _temperature_scale = oversample_scalefactor[os];

    // Set shift bit if necessary
    Adafruit_BusIO_Register CFG_REG = Adafruit_BusIO_Register(
        i2c_dev, 
        DPS310_CFGREG, 
        1,
        LSBFIRST,
        1);
  
    if ( ! Adafruit_BusIO_RegisterBits(
        &CFG_REG, 
        1, 
        3).write( (os > DPS310_8SAMPLES) ? 0x01 : 0x00 ) ) {
            return false;
    }

    // REad the MSB from Coef Src register: ASIC or MEMS?
    Adafruit_BusIO_Register TMP_COEFF = Adafruit_BusIO_Register(
        i2c_dev, 
        DPS310_TMPCOEFSRCE, 
        1,
        LSBFIRST,
        1);

    uint32_t val = 0;
    if ( !Adafruit_BusIO_RegisterBits(
        &TMP_COEFF, 
        1, 
        7).read(&val) ) {
            return false;
    }

    // and set the configuration from the read value
    if ( !Adafruit_BusIO_RegisterBits(
        &TMP_CFG, 
        1, 
        7).write(val) ) {
            return false;
    }

    return true;
}

/*
  @brief  Read the data from the sensor and store in the internal variables
*/
bool Adafruit_DPS310::readMeasurements(dps310_sensor_data* data) {

    // read raw temperature
    uint32_t valTemp = 0;
    if ( ! Adafruit_BusIO_Register(
        i2c_dev, 
        DPS310_TMPB2, 
        3, 
        MSBFIRST,
        1).read( &valTemp ) ) {
            return false;
    }

    // read raw pressure
    uint32_t valPressure = 0;
    if ( ! Adafruit_BusIO_Register(
        i2c_dev, 
        DPS310_PRSB2, 
        3, 
        MSBFIRST,
        1).read( &valPressure ) ) {
            return false;
    }

    // compute temperature
    _raw_temperature = twosComplement(valTemp, 24);
    float traw_sc = ((float)_raw_temperature) / _temperature_scale;
    _temperature = 0.5 * c0 + traw_sc * c1;

    data->temperature = _temperature;

    // compute pressure
    _raw_pressure = twosComplement(valPressure, 24);
    float praw_sc = ((float)_raw_pressure) / _pressure_scale;

    float d0 = c20 + praw_sc + c30;
    float d1 = c10 + praw_sc + d0;
    float d2 = c11 + praw_sc + c21;
    _pressure = c00 + praw_sc * d1 + traw_sc * c01 + traw_sc * praw_sc * d2;

    data->pressure = _pressure;

    return true;
}
