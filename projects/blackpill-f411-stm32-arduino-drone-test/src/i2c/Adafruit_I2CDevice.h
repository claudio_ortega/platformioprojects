#ifndef Adafruit_I2CDevice_h
#define Adafruit_I2CDevice_h

#include "i2c/Wire2.h"

///< The class which defines how we will talk to this device over I2C
class Adafruit_I2CDevice {
    public:
        Adafruit_I2CDevice(uint8_t addr, TwoWire2 *theWire);
        bool detected();
        bool read(uint8_t *buffer, size_t len, bool stop);
        bool write(const uint8_t *buffer, size_t len, bool stop, const uint8_t *prefix_buffer, size_t prefix_len);
        bool write_then_read(const uint8_t *write_buffer, size_t write_len, uint8_t *read_buffer, size_t read_len, bool stop);

    private:
        uint8_t _addr;
        TwoWire2 *_wire;
        size_t _maxBufferSize;
};

#endif // Adafruit_I2CDevice_h
