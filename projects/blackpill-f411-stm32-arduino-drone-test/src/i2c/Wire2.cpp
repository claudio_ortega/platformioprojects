/*
  Wire2.cpp - TWI/I2C library for Wiring & Arduino
  Copyright (c) 2006 Nicholas Zambetti.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  Modified 2012 by Todd Krein (todd@krein.org) to implement repeated starts
*/

extern "C" {
    #include <stdlib.h>
    #include <string.h>
    #include <inttypes.h>
}

#include "i2c/Wire2.h"

// Distinguish master from slave.
// 0x01 is a reserved value, and thus cannot be used by slave devices
static const uint8_t BUFFER_LENGTH = 32;

TwoWire2::TwoWire2(uint8_t sda, uint8_t scl)
{
    memset( &_i2c, 0 , sizeof(_i2c) );
    _i2c.sda = digitalPinToPinName(sda);
    _i2c.scl = digitalPinToPinName(scl);

    rxBuffer = nullptr;
    rxBufferAllocated = 0;
    rxBufferIndex = 0;
    rxBufferLength = 0;

    txAddress = 0;
    txBuffer = nullptr;
    txBufferAllocated = 0;
    txBufferIndex = 0;
    txBufferLength = 0;

    transmitting = false;
}

void TwoWire2::begin(uint8_t address, bool generalCall, int clockInHz)
{
    rxBufferIndex = 0;
    rxBufferLength = 0;
    rxBuffer = NULL;
    rxBufferAllocated = 0;

    txBufferIndex = 0;
    txBufferLength = 0;
    txAddress = 0;
    txBuffer = NULL;
    txBufferAllocated = 0;

    _i2c.__this = (void *)this;
    transmitting = false;

    _i2c.isMaster = (address == MASTER_ADDRESS) ? 1 : 0;
    _i2c.generalCall = (generalCall == true) ? 1 : 0;

    uint8_t ownAddress = address << 1;
    i2c_custom_init(&_i2c, clockInHz, I2C_ADDRESSINGMODE_7BIT, ownAddress);

    if (_i2c.isMaster == 0) {
        i2c_attachSlaveTxEvent(&_i2c, NULL);
        i2c_attachSlaveRxEvent(&_i2c, NULL);
    }
}

void TwoWire2::end()
{
    i2c_deinit(&_i2c);
    free(txBuffer);
    txBuffer = NULL;
    txBufferAllocated = 0;
    free(rxBuffer);
    rxBuffer = NULL;
    rxBufferAllocated = 0;
}

uint8_t TwoWire2::requestFrom(uint8_t address, uint8_t quantity, uint32_t iaddress, uint8_t isize, uint8_t sendStop)
{
    uint8_t read = 0;

    if (_i2c.isMaster == 1) {

        if (!allocateRxBuffer(quantity)) {
            return 0;
        }

        // error if no memory block available to allocate the buffer
        if (rxBuffer != NULL) {
            if (isize > 0) {
                // send internal address; this mode allows sending a repeated start to access
                // some devices' internal registers. This function is executed by the hardware
                // TWI module on other processors (for example Due's TWI_IADR and TWI_MMR registers)

                beginTransmission(address);

                // the maximum size of internal address is 3 bytes
                if (isize > 3) {
                isize = 3;
                }

                // write internal register address - most significant byte first
                while (isize-- > 0) {
                write((uint8_t)(iaddress >> (isize * 8)));
                }
                endTransmission(false);
            }

        // perform blocking read into buffer
#if defined(I2C_OTHER_FRAME)
            if (sendStop == 0) {
                _i2c.handle.XferOptions = I2C_OTHER_FRAME ;
            } else {
                _i2c.handle.XferOptions = I2C_OTHER_AND_LAST_FRAME;
            }
#endif
            if (I2C_OK == i2c_master_read(&_i2c, address << 1, rxBuffer, quantity)) {
                read = quantity;
            }

            // set rx buffer iterator vars
            rxBufferIndex = 0;
            rxBufferLength = read;
        }
    }
    
    return read;
}

uint8_t TwoWire2::requestFrom(uint8_t address, uint8_t quantity, uint8_t sendStop)
{
    return requestFrom(
        (uint8_t)address, 
        (uint8_t)quantity, 
        (uint32_t)0, 
        (uint8_t)0, 
        (uint8_t)sendStop);
}

void TwoWire2::beginTransmission(uint8_t address)
{
    // indicate that we are transmitting
    transmitting = true;
    // set address of targeted slave
    txAddress = address << 1;
    // reset tx buffer iterator vars
    txBufferIndex = 0;
    txBufferLength = 0;
}

//
//  Originally, 'endTransmission' was an f() function.
//  It has been modified to take one parameter indicating
//  whether or not a STOP should be performed on the bus.
//  Calling endTransmission(false) allows a sketch to
//  perform a repeated start.
//
//  WARNING: Nothing in the library keeps track of whether
//  the bus tenure has been properly ended with a STOP. It
//  is very possible to leave the bus in a hung state if
//  no call to endTransmission(true) is made. Some I2C
//  devices will behave oddly if they do not see a STOP.
//
uint8_t TwoWire2::endTransmission(bool sendStop)
{

  // check transfer options and store it in the I2C handle
#if defined(I2C_OTHER_FRAME)
    if ( !sendStop ) {
        _i2c.handle.XferOptions = I2C_OTHER_FRAME ;
    } else {
        _i2c.handle.XferOptions = I2C_OTHER_AND_LAST_FRAME;
    }
#endif

    int8_t ret = 4;
    if (_i2c.isMaster == 1) {
        // transmit buffer (blocking)
        switch (i2c_master_write(&_i2c, txAddress, txBuffer, txBufferLength)) {
            case I2C_OK :
                ret = 0; // Success
                break;
            case I2C_DATA_TOO_LONG :
                ret = 1;
                break;
            case I2C_NACK_ADDR:
                ret = 2;
                break;
            case I2C_NACK_DATA:
                ret = 3;
                break;
            case I2C_TIMEOUT:
            case I2C_BUSY:
            case I2C_ERROR:
            default:
                ret = 4;
                break;
        }

        // reset Tx buffer
        resetTxBuffer();

        // reset tx buffer iterator vars
        txBufferIndex = 0;
        txBufferLength = 0;

        // indicate that we are done transmitting
        transmitting = false;
    }

  return ret;
}

// must be called in:
// slave tx event callback
// or after beginTransmission(address)
size_t TwoWire2::write(uint8_t data)
{
    size_t ret = 1;
    if (transmitting) {
        // in master transmitter mode
        allocateTxBuffer(txBufferLength + 1);
        // error if no memory block available to allocate the buffer
        if (txBuffer == NULL) {
            ret = 0;
        } else {
            // put byte in tx buffer
            txBuffer[txBufferIndex] = data;
            ++txBufferIndex;
            // update amount in buffer
            txBufferLength = txBufferIndex;
        }
    } 
    else {
        // in slave send mode
        // reply to master
        if (i2c_slave_write_IT(&_i2c, &data, 1) != I2C_OK) {
            ret = 0;
        }
    }
    return ret;
}

/**
  * @brief  This function must be called in slave Tx event callback or after
  *         beginTransmission() and before endTransmission().
  * @param  pdata: pointer to the buffer data
  * @param  quantity: number of bytes to write
  * @retval number of bytes ready to write.
  */
size_t TwoWire2::write(const uint8_t *data, size_t quantity)
{
    size_t ret = quantity;

    if (transmitting) {
        // in master transmitter mode
        allocateTxBuffer(txBufferLength + quantity);
        // error if no memory block available to allocate the buffer
        if (txBuffer == NULL) {
            ret = 0;
        } else {
            // put bytes in tx buffer
            memcpy(&(txBuffer[txBufferIndex]), data, quantity);
            txBufferIndex = txBufferIndex + quantity;
            // update amount in buffer
            txBufferLength = txBufferIndex;
        }
    } 
    else {
        // in slave send mode
        // reply to master
        if (i2c_slave_write_IT(&_i2c, (uint8_t *)data, quantity) != I2C_OK) {
            ret = 0;
        }
    }
    return ret;
}

// must be called in:
// slave rx event callback
// or after requestFrom(address, numBytes)
int TwoWire2::available()
{
    return rxBufferLength - rxBufferIndex;
}

// must be called in:
// slave rx event callback
// or after requestFrom(address, numBytes)
int TwoWire2::read()
{
    int value = -1;

    // get each successive byte on each call
    if (rxBufferIndex < rxBufferLength) {
        value = rxBuffer[rxBufferIndex];
        ++rxBufferIndex;
    }

    return value;
}

// must be called in:
// slave rx event callback
// or after requestFrom(address, numBytes)
int TwoWire2::peek()
{
    int value = -1;

    if (rxBufferIndex < rxBufferLength) {
        value = rxBuffer[rxBufferIndex];
    }

    return value;
}

void TwoWire2::flush()
{
    rxBufferIndex = 0;
    rxBufferLength = 0;
    resetRxBuffer();
    txBufferIndex = 0;
    txBufferLength = 0;
    resetTxBuffer();
}

/**
  * @brief  Allocate the Rx/Tx buffer to the requested length if needed
  * @note   Minimum allocated size is BUFFER_LENGTH)
  * @param  length: number of bytes to allocate
  */
bool TwoWire2::allocateRxBuffer(size_t length)
{
    if (rxBufferAllocated < length) {
        // By default we allocate BUFFER_LENGTH bytes. It is the min size of the buffer.
        if (length < BUFFER_LENGTH) {
            length = BUFFER_LENGTH;
        }

        uint8_t *tmp = (uint8_t *)realloc(rxBuffer, length * sizeof(uint8_t));
        if (tmp == NULL) {
            return false;
        } 
        rxBuffer = tmp;
        rxBufferAllocated = length;
    }

    return true;
}

bool TwoWire2::allocateTxBuffer(size_t length)
{
    if (txBufferAllocated < length) {
        // By default we allocate BUFFER_LENGTH bytes. It is the min size of the buffer.
        if (length < BUFFER_LENGTH) {
            length = BUFFER_LENGTH;
        }
        uint8_t *tmp = (uint8_t *)realloc(txBuffer, length * sizeof(uint8_t));
        if (tmp == NULL) {
            return false;
        }
        txBuffer = tmp;
        txBufferAllocated = length;
    }
    return true;
}

/**
  * @brief  Reset Rx/Tx buffer content to 0
  */
void TwoWire2::resetRxBuffer()
{
    if (rxBuffer != NULL) {
        memset(rxBuffer, 0, rxBufferAllocated);
    }
}

void TwoWire2::resetTxBuffer()
{
    if (txBuffer != NULL) {
        memset(txBuffer, 0, txBufferAllocated);
    }
}

