#include <Arduino.h>
#include "common/system2.hpp"
#include "test/testListI2C.hpp"
#include "test/testAll.hpp"
#include "common/logger.hpp"

// called from main() we do not need/want to do anything in here,
// see ~/.platformio/packages/framework-arduinoststm32/cores/arduino/main.cpp
void initVariant() {}

// called from main() after returning from setup(),
// but as we are not returning from setup(), then we do not need to do anything in here
// as it is never reached,
// see ~/.platformio/packages/framework-arduinoststm32/cores/arduino/main.cpp
void loop() {}

// setup() is called from main(), and never returns,
// this app is totally interrupt driven
void setup() {
    
    static HardwareSerial *debugUART = new HardwareSerial(PA_10, PA_9);
    debugUART->begin(115200);

    logger::print(debugUART, "");
    logger::print(debugUART, "CPU clock freq: %f MHz", SystemCoreClock/1000000.0);

    bool initStatus = true;

    switch ( 1 ) {
        case 0:
            testListI2C::init(debugUART);
            break;
        case 1:
            initStatus = testAll::init(
                debugUART,
                false,         // enable log
                true,          // enable pressure - DPS310
                true,          // enable imu - LSM6DS
                true,          // enable mag - LIS3MDL
                true,          // enable UART_RX
                true,          // enable UART_TX
                true,          // enable pwm
                1000000,       // I2C clock (hertz)
                5000,          // main loop period (microsec)
                10000,         // compute size
                1,             // telemetry decimation factor
                2000);         // print decimation factor 
            break;
        default:
            initStatus = false;
            break;    
    }

    logger::print( debugUART, initStatus ? "init() succeeded" : "init() failed" );

    while(1){}
}


