#include <Arduino.h>
#include "common/logger.hpp"
#include "common/pwm.hpp"  
#include "common/system2.hpp"
#include "common/telemetry.hpp"
#include "test/testAll.hpp"
#include "i2c/Wire2.h"
#include "sensors/Adafruit_DPS310.h"
#include "sensors/Adafruit_LSM6DS.h"
#include "sensors/Adafruit_LIS3MDL.h"
#include <PinNames.h>

namespace testAll {

    bool printOut = false;
    bool usePressure = false;
    bool useImu = false;
    bool useMag = false;
    bool useUART_RX = false;
    bool useUART_TX = false;
    bool usePwm = false;
    int periodMicroSec = 0;
    int telemetryDecimationFactor = 0;
    int printDecimationFactor = 0;
    bool controlLoopEnabled = false;

    HardwareSerial* serialDebug = nullptr;
    HardwareSerial* serial2 = nullptr;
    HardwareSerial* serial3 = nullptr;

    HardwareTimer* timer = nullptr;
    TwoWire2* wire = nullptr;
    Adafruit_LSM6DS* lsm6ds = nullptr;
    Adafruit_LIS3MDL* lis3mdl = nullptr;
    Adafruit_DPS310* dps310 = nullptr;
    Arduino_CRC32 *crc32 = nullptr;

    const int pwmCount = 4;
    Pwm *pPwm[pwmCount] = {nullptr};
    const uint32_t testPin1 = pinNametoDigitalPin(PA_5); 
    const uint32_t testPin2 = pinNametoDigitalPin(PA_6);
    const uint32_t testPin3 = pinNametoDigitalPin(PA_7);
    // PC_13 is connected via a resistor to the blue LED in the black pill board, and is active low
    const uint32_t indicationPin = pinNametoDigitalPin(PC_13);

    const int telemetry_tx_size = 18;
    float telemetry_tx[telemetry_tx_size] = {0};

    typedef struct {
        float* a;
        float* b;
        int size;
    } computationData;

    computationData computation_data = {0};

    typedef enum {
        BLINK_OFF,
        BLINK_ON,
        BLINK_FAST,
        BLINK_SLOW,
        BLINK_BEACON
    } BlinkEnum;

    static BlinkEnum blink_control_enum = BLINK_OFF;

    inline bool shouldPrint( bool printOut, uint64_t loopCount, int printLoops ) {
        return printOut && ( loopCount % printLoops == 0 );
    }

    static float updateComputation(computationData* c) {
        float sum = 0.0;
        for ( int i=0; i<c->size; i++ ) {
            sum = sum + (c->a[i] * c->b[i]);
        }
        return sum;
    }

    static void updateImu( uint64_t loopCount, float telemetry_tx_payload[]) {

        static xyz_triplet_t accel = {0}; 
        static xyz_triplet_t gyro = {0}; 
        static uint64_t errCount = 0; 

        const bool status = lsm6ds->pollRegisters();
        if ( !status ) {
            errCount++;
        }

        lsm6ds->getAccel(&accel);
        lsm6ds->getGyro(&gyro);

        if ( shouldPrint( printOut, loopCount, printDecimationFactor ) ) {
            logger::print (serialDebug, "accel->x: [%f], accel->y:[%f], accel->z:[%f]", accel.x, accel.y, accel.z );
            logger::print (serialDebug, "gyro->x: [%f], gyro->y: [%f], gyro->z: [%f]", gyro.x, gyro.y, gyro.z );
            logger::print (serialDebug, "errCount: [%d]", errCount );
            logger::print (serialDebug, "" );
        }

        telemetry_tx_payload[0] = accel.x;
        telemetry_tx_payload[1] = accel.y;
        telemetry_tx_payload[2] = accel.z;
        
        telemetry_tx_payload[3] = gyro.x;
        telemetry_tx_payload[4] = gyro.y;
        telemetry_tx_payload[5] = gyro.z;        
    }

    static void updateMag( uint64_t loopCount, float telemetry_tx_payload[] ) {
        
        static xyz_mag_triplet mag = {0};
        static uint32_t errCount = 0; 

        const bool status = lis3mdl->pollRegisters();  
        if ( !status ) {
            errCount++;
        }

        lis3mdl->getMag(&mag);  
        
        if ( shouldPrint( printOut, loopCount, printDecimationFactor ) ) {
            logger::print (serialDebug, "mag->x: [%f], mag->y:[%f], mag->z:[%f]", mag.x, mag.y, mag.z );
            logger::print (serialDebug, "errCount: [%d]", errCount );
            logger::print (serialDebug, "" );
        }

        telemetry_tx_payload[6] = mag.x;
        telemetry_tx_payload[7] = mag.y;
        telemetry_tx_payload[8] = mag.z;
    }

    static void updatePressure( uint64_t loopCount, float telemetry_tx_payload[] ) {

        static dps310_sensor_data measurement = {0};
        static uint64_t errCount = 0; 
        
        const bool ok = dps310->readMeasurements(&measurement);
        const float altitude = 44330.8 - 4946.54 * pow ( measurement.pressure / 100.0, 0.1902632 );

        if ( !ok ) {
            errCount++;
        }

        if ( shouldPrint( printOut, loopCount, printDecimationFactor ) ) {
            logger::print (serialDebug, "temperature (cent): [%f], pressure (hPa): [%f], altitude (m): [%f]", 
                measurement.temperature,
                measurement.pressure,
                altitude );
            logger::print (serialDebug, "errCount: [%d]", errCount );
            logger::print (serialDebug, "" );
        }

        telemetry_tx_payload[9] = measurement.temperature;
        telemetry_tx_payload[10] = measurement.pressure;
        telemetry_tx_payload[11] = altitude;
    }

    static void updatePwm( uint64_t loopCount ) {
        const uint32_t widthInMicroSec = (10*loopCount) % 10000;
        for ( int i=0; i<pwmCount; i++ ) {
            pPwm[i]->setDurationInMicroSec(widthInMicroSec );
        }
    }

    static void updateUART_TX( uint64_t loopCount, float telemetry_tx_payload[] ) {
        telemetry::print( 
            serial2, 
            crc32, 
            loopCount/telemetryDecimationFactor, 
            telemetry_tx_payload, 
            telemetry_tx_size );
    }

    static void updateUART_RX( uint64_t loopCount ) {
        while( serial2->available() ) {
            const uint8_t rx = (const uint8_t) serial2->read();
            logger::print (serialDebug, "rx2:[%x]", rx );
        }

        while( serial3->available() ) {
            const uint8_t rx = (const uint8_t) serial3->read();
            logger::print (serialDebug, "rx3:[%x]", rx );
        }
    }

    static void showActivityOnLED() {
        static int interrupt_count = 0;
        const int divisor = 1;
        interrupt_count++;
        int decimatedCount = interrupt_count/divisor;
        switch ( blink_control_enum ) {
            case BLINK_OFF:
                digitalWrite(indicationPin, HIGH);
                break;
            case BLINK_ON:
                digitalWrite(indicationPin, LOW);
                break;
            case BLINK_BEACON:
                digitalWrite(indicationPin, ((decimatedCount/5)%30) == 0 ? LOW : HIGH);
                break;
            case BLINK_FAST:
                digitalWrite(indicationPin, ((decimatedCount/10)%2) == 0 ? LOW : HIGH);
                break;
            case BLINK_SLOW:
                digitalWrite(indicationPin, ((decimatedCount/100)%2) == 0 ? LOW : HIGH);
                break;
        }
    }

    static void controlLoop() {

        static uint32_t loopCount = 0;

        loopCount++;

        digitalWrite(testPin1, 1);
        if ( useUART_TX && loopCount % telemetryDecimationFactor == 0 ) {
            digitalWrite(testPin2, 1);
        }
        
        if (useImu) {
            updateImu(loopCount, telemetry_tx);
        }
       
        if (useMag) {
            updateMag(loopCount, telemetry_tx);
        }
       
        if (usePressure) {
            updatePressure(loopCount, telemetry_tx);
        }
       
        if ( usePwm ) {
            updatePwm(loopCount);
        }
        
        if ( useUART_RX ) {
            updateUART_RX(loopCount);
        }

        if ( useUART_TX && loopCount % telemetryDecimationFactor == 0 ) {
            updateUART_TX(loopCount, telemetry_tx);
        }

        digitalWrite(testPin3, 1);
        volatile float sum = updateComputation(&computation_data);
        digitalWrite(testPin3, 0);

        if ( shouldPrint( printOut, loopCount, printDecimationFactor ) ) {
            logger::print ( serialDebug, "loopCount:%d, computeSize:%d, sum:%f", loopCount, computation_data.size, sum );
            logger::print ( serialDebug, "" );                
        }

        if ( useUART_TX && loopCount % telemetryDecimationFactor == 0 ) {
            digitalWrite(testPin2, 0);
        }

        digitalWrite(testPin1, 0);
    }

    static void initPwm() {
        const int pwmFrequency = 50;       
        static const PinName pwmPins[pwmCount] = {PB_4, PB_5, PB_6, PB_7};
        for ( int i=0; i<sizeof(pwmPins)/sizeof(pwmPins[0]); i++ ) {
            pPwm[i] = new Pwm();
            pPwm[i]->init(pwmPins[i], pwmFrequency );
        }
    }

    static void initUART() {
        serial2 = new HardwareSerial(PA_3, PA_2);
        serial2->begin(921600);

        serial3 = new HardwareSerial(PA_12, PA_11);
        serial3->begin(115200);

        crc32 = new Arduino_CRC32();
    }

    static bool initImu( TwoWire2* twoWire ) {
        
        lsm6ds = new Adafruit_LSM6DS();

        if (! lsm6ds->begin_I2C(Adafruit_LSM6DS::LSM6DS_I2CADDR_SECONDARY, twoWire)) {         
            return false;
        }

        if (!lsm6ds->init() ) {
            return false;
        }

        return true;
    }
 
    static bool initMag( TwoWire2* twoWire ) {

        lis3mdl = new Adafruit_LIS3MDL();

        if ( !lis3mdl->begin_I2C(Adafruit_LIS3MDL::LIS3MDL_I2CADDR_SECONDARY, twoWire) ) {
            return false;
        }

        if ( !lis3mdl->init() ) {
            return false;
        }

        return true;
    }

    static bool initPressure( TwoWire2* twoWire ) {

        dps310 = new Adafruit_DPS310();

        if ( ! dps310->begin_I2C(Adafruit_DPS310::DPS310_I2CADDR_PRIMARY, twoWire) ) {
            return false;
        }

        if ( ! dps310->init() ) {
            return false;
        }

        return true;
    }

    static void initComputation(computationData* c, int size) {
        c->size = size;
        c->a = new float[size];
        c->b = new float[size];
        for ( int i=0; i<size; i++ ) {
            c->a[i] = (float)(i+1);
            c->b[i] = 1.0/(float)(i+1);
        }
    }

    static void timerCallback() {
        showActivityOnLED();
        if (controlLoopEnabled) {
            controlLoop();
        }
    }

    static void initTimer( int periodMicroSec ) {
        timer = new HardwareTimer(TIM1);
        timer->setOverflow(periodMicroSec, MICROSEC_FORMAT);
        timer->attachInterrupt(timerCallback);
        timer->resume();
    }

    bool init(
        HardwareSerial* inSerialDebug,
        bool inPrintOut,
        bool inUsePressure,
        bool inUseImu,
        bool inUseMag,
        bool inUseUART_RX,
        bool inUseUART_TX,
        bool inUsePwm,
        int clockI2C,
        int inPeriodMicroSec,
        int inComputeSize,
        int inTelemetryDecimationFactor,
        int inPrintDecimationFactor ) {
            
        serialDebug = inSerialDebug;
        printOut = inPrintOut;
        usePressure = inUsePressure;
        useImu = inUseImu;
        useMag = inUseMag;
        useUART_RX = inUseUART_RX;
        useUART_TX = inUseUART_TX;
        usePwm = inUsePwm;
        periodMicroSec = inPeriodMicroSec;
        telemetryDecimationFactor = inTelemetryDecimationFactor;
        printDecimationFactor = inPrintDecimationFactor;

        pinMode(testPin1, OUTPUT);
        pinMode(testPin2, OUTPUT);
        pinMode(testPin3, OUTPUT);
        pinMode(indicationPin, OUTPUT);

        blink_control_enum = BLINK_SLOW;
        initTimer( periodMicroSec );
        logger::print (serialDebug, "waiting 5 secs for initialization to start()");
        delay(5000);

        wire = new TwoWire2(PB_9, PB_8);

        if ( useImu || useMag || usePressure ) {
            wire->begin(TwoWire2::MASTER_ADDRESS, false, clockI2C);
        }

        int initErrCount = 0;

        if ( useImu ) {
            if ( !initImu( wire ) )  {
                logger::print (serialDebug, "imu sensor init failed");
                initErrCount++;
            }
        }
       
        if ( useMag ) {
            if ( !initMag( wire ) )  {
                logger::print (serialDebug, "mag sensor init failed");
                initErrCount++;
            }
        }
       
        if ( usePressure ) {
            if ( !initPressure( wire ) )  {
                logger::print (serialDebug, "pressure sensor init failed");
                initErrCount++;
            }
        }

        if ( initErrCount > 0 ) {
            blink_control_enum = BLINK_FAST;
            return false;
        }

        if ( usePwm ) {
            initPwm();
        }
        
        if ( useUART_RX || useUART_TX ) {
            initUART();
        }

        initComputation(&computation_data, inComputeSize);

        blink_control_enum = BLINK_BEACON;
        controlLoopEnabled = true;

        return true;
    }
}
