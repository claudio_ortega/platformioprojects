#include <Arduino.h>
#include "common/logger.hpp"
#include "common/pwm.hpp"                  
#include "test/testPwm.hpp"
#include "pins_arduino.h"           // PIN mappings

namespace testPwm {

    static const PinName pwmPins[4] = {PB_4, PB_6, PB_3, PB_5};

    static Pwm *pwms[4] = {NULL};

    void update_IT_callback() { 
        static uint64_t loopCount = 0;
        ++loopCount;
        const uint32_t widthInMicroSec = loopCount % 1000;
        for ( int i=0; i<4; i++ ) {
            pwms[i]->setDurationInMicroSec( widthInMicroSec );
        }
    }

    void initPwm() {
        for ( int i=0; i<4; i++ ) {
            pwms[i] = new Pwm();
            pwms[i]->init( pwmPins[i], 1000 );
        }
    }

    void initTimer() {
        HardwareTimer* const timer = new HardwareTimer(TIM1);
        timer->attachInterrupt(update_IT_callback);
        timer->setOverflow(100, HERTZ_FORMAT); 
        timer->resume();
    }

    void init() {
        initPwm();
        initTimer();
    }
}
