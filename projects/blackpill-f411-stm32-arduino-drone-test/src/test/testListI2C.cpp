#include <Arduino.h>
#include "common/logger.hpp"
#include "i2c/Wire2.h"
#include "test/testListI2C.hpp"

namespace testListI2C {

    HardwareSerial* serialDebug = nullptr;
    TwoWire2* wire = nullptr;
    uint32_t testPin = 0;      

    void timerCallback() { 
        
        const int clockI2CHz = 1000000;
        const int LAST_ADDRESS = 0x7F;
        const int FIRST_LOOP = 10;
        const int LAST_LOOP = FIRST_LOOP + LAST_ADDRESS;
        
        // any printing will alter the timing in the logic analyzer results,
        // so we keep all responsive addresses in an array, and we print them later 
        static int responsive[0x80] = {0};
        static int nextEmptyIndex = 0;
        static bool done = false;
        static uint64_t loopCount = 0;

        if ( done ) {
            return;
        }

        // avoid the first burst of timer ticks, Serial won't work!!
        loopCount++;

        if ( loopCount == FIRST_LOOP ) {
            logger::print (serialDebug, "scan -- BEGIN");
            digitalWrite(testPin, 1);
            wire->begin(TwoWire2::MASTER_ADDRESS, false, clockI2CHz);
            digitalWrite(testPin, 0);
        }

        else if ( loopCount > FIRST_LOOP && loopCount < (LAST_LOOP+2) ) {
            const int address = loopCount - FIRST_LOOP - 1;
            digitalWrite(testPin, 1);
            wire->beginTransmission((uint8_t)address);
            const uint8_t error = wire->endTransmission(1);
            digitalWrite(testPin, 0);
            if ( error == 0 ) {
                responsive[nextEmptyIndex] = address;
                nextEmptyIndex++;
            }
        }

        else if ( loopCount == (LAST_LOOP+2) ) {

            for ( int i=0; i<nextEmptyIndex; i++ ) {
                logger::print (serialDebug, "found I2C at: 0x%x", responsive[i]);
            }

            logger::print (serialDebug, "scan -- END");
            done = true;
        }
    }

    void init( HardwareSerial* inSerialDebug ) {

        serialDebug = inSerialDebug;

        logger::print (serialDebug, "init() -- BEGIN");

        wire = new TwoWire2(PB_9, PB_8);
        testPin = pinNametoDigitalPin(PA_5);
        pinMode(testPin, OUTPUT);

        HardwareTimer* timer = new HardwareTimer(TIM1);
        timer->setOverflow(500, MICROSEC_FORMAT);
        timer->attachInterrupt(timerCallback);
        timer->resume();

        logger::print (serialDebug, "init() -- END");
    }
}
