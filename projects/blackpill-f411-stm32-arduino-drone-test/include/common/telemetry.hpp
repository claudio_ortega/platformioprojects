#ifndef __telemetry_hpp__
#define __telemetry_hpp__

#include <Arduino.h>
#include <Arduino_CRC32.h>

namespace telemetry {
    void print( HardwareSerial* serial, Arduino_CRC32* crc32, const uint64_t time_index, const float* in, const uint16_t len );
}

#endif
