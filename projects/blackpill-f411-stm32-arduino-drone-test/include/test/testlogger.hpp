#ifndef __testLogger_hpp__
#define __testLogger_hpp__

#include <Arduino.h>

namespace testLogger {
    bool init (HardwareSerial* inSerialDebug);
}

#endif
