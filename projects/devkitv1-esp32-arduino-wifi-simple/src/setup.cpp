#include <Arduino.h>
#include <WiFi.h>

// GPIO_2 is connected into the blue LED in the board
const int GPIO_BLUE_LED_PIN = 2;

const char* ap_ssid = "ortitos";
const char* ap_password = "noelia2034";

typedef enum { 
    BLINK_OFF, 
    BLINK_ON, 
    BLINK_FAST, 
    BLINK_SLOW, 
    BLINK_BEACON 
} BlinkEnum;

volatile BlinkEnum blink_control_enum = BLINK_OFF;

void IRAM_ATTR onTimer() {
    static int interrupt_count = 0;
    interrupt_count++;
    int scaled_count = interrupt_count / 5;
    switch ( blink_control_enum ) {
        case BLINK_OFF:
            digitalWrite(GPIO_BLUE_LED_PIN, LOW);
            break;
        case BLINK_ON:
            digitalWrite(GPIO_BLUE_LED_PIN, HIGH);
            break;
        case BLINK_BEACON:
            digitalWrite(GPIO_BLUE_LED_PIN, ((scaled_count)%30) == 0 ? HIGH : LOW);
            break;
        case BLINK_FAST:
            digitalWrite(GPIO_BLUE_LED_PIN, ((scaled_count)%2) == 0 ? HIGH : LOW);
            break;
        case BLINK_SLOW:
            digitalWrite(GPIO_BLUE_LED_PIN, ((scaled_count/20)%2) == 0 ? HIGH : LOW);
            break;
    }
}

// called from main() we do not need/want to do anything in here,
void initVariant() {}

// called from main() after returning from setup(),
// but as we are not returning from setup(), then we do not need to do anything in here
// as it is never reached,
void loop() {}

void halt() {
    blink_control_enum = BLINK_FAST;
    while(true){}
}

void connect() {

    // for some obscure reason this delay seems to be needed
    Serial.printf( "waiting for 2 seconds\n");
    delay(1000);

    const bool ok1 = WiFi.mode(WIFI_STA);
    Serial.printf( "mode(WIFI_STA) %s\n", ok1 ? "succeeded" : "failed");
    if ( !ok1 ) {
        halt();
    }

    delay(1000);

    const bool ok2 =  WiFi.begin(ap_ssid, ap_password);
    Serial.printf("begin(%s, %s) %s\n", ap_ssid, ap_password, ok2 ? "succeeded" : "failed");
    if ( !ok2 ) {
        halt();
    }

    static int count = 0;
    while ( WiFi.status() != WL_CONNECTED ) {
        Serial.printf(".");
        delay(1000);
        if (count++ > 20) {
            Serial.printf("\nrestart!\n");
            delay(3000);
            ESP.restart();
        }
    }

    Serial.printf( "connected\n");
}

void setup() {
    Serial.begin(115200);
    Serial.println("\n\n\nprogram started");

    // setup the blue onboard LED
    pinMode(GPIO_BLUE_LED_PIN, OUTPUT);
    digitalWrite(GPIO_BLUE_LED_PIN, LOW);

    // indicate this way that things are still pending
    // (1) select tick period to be 100uSec, f=10KHz, from fClock=80MHz => divisor=8000
    hw_timer_t* timer = timerBegin(0, 8000, true);
    // (2) hit on callback onTimer() every 10mSec, so then we want 1 every 100 ticks
    timerAlarmWrite(timer, 100, true);
    timerAttachInterrupt(timer, &onTimer, true);
 
    connect();

    blink_control_enum = BLINK_BEACON;

    // has to happen after connect()
    timerAlarmEnable(timer);

    while (true) {
        Serial.printf("RSSI:%d\n", WiFi.RSSI());
        delay(1000);
    }
}



