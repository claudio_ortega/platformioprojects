#ifndef __fix_sizer_hpp__
#define __fix_sizer_hpp__

#include <Arduino.h>

class Buffer {
    public:
        Buffer(int inSize);
        bool add( uint8_t* in, int n );
        int available();
        int roomLeft();
        bool get( uint8_t* );
        void reset();
    private:
        int size;
        int count;
        uint8_t* buffer;
};

class Chopper {
    public:
        Chopper(int inSize);
        bool add( uint8_t* in, int n );
        bool available();
        int roomLeft();
        bool get( uint8_t* );
        void reset();
        void printDebugInfo(const char* tag);
    private:
        int size;
        Buffer a;
        Buffer b;
};

#endif
