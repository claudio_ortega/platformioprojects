#include "chopper.hpp"

Buffer::Buffer(int maxSize):
    size(maxSize),
    count(0),
    buffer(new uint8_t[maxSize]) {
}

bool Buffer::add( uint8_t* in, int n ) {
    if ( ( count + n ) > size ) {
        Serial.printf("ERROR(1): count:%d, n:%d, size:%d\n", count, n, size);
        return false;
    }
    memcpy( buffer + count, in, n );
    count = count + n;
    return true;
}

bool Buffer::get(uint8_t* inout) {
    if ( count != size ) {
        Serial.printf("ERROR(2): count:%d, size:%d\n", count, size);
        return false;
    }

    memcpy( inout, buffer, size );
    count = 0;

    return true;
}

void Buffer::reset() {
    count = 0;
}

int Buffer::available() {
    return count;
}

int Buffer::roomLeft() {
    if ( count > size ) {
        Serial.printf("ERROR(3): count:%d, size:%d\n", count, size);
    }
    return size - count;
}

// ----------------------------------------------------------------------------------------

Chopper::Chopper(int maxSize):
    size(maxSize),
    a(maxSize),
    b(maxSize) {
}

bool Chopper::add( uint8_t* in, int n ) {
    
    if ( n == 0 ) {
        Serial.printf("n==0\n");
        return false;
    }

    if ( a.available() > 0 && b.available() > 0 ) {
        Serial.printf("ERROR(4.1): wrong state, n: %d, a.available: %d, b.available: %d\n", n, a.available(), b.available());
        a.reset();
        b.reset();
        return false;
    }

    if ( a.available() > size || b.available() > size ) {
        Serial.printf("ERROR(4.2): wrong state, n: %d, a.available: %d, b.available: %d\n", n, a.available(), b.available());
        a.reset();
        b.reset();
        return false;
    }

    if ( n > (a.roomLeft() + b.roomLeft()) ) {
        Serial.printf("ERROR(4.3): buffer overflow, n: %d, a.roomLeft: %d, b.roomLeft: %d\n", n, a.roomLeft(), b.roomLeft());
        a.reset();
        b.reset();
        return false;
    }

    // the component with more stuff already in it is the candidate to fill in next
    Buffer *first = &a;
    Buffer *second = &b;

    if ( a.available() == 0 && b.available() == 0 ) {
        first = &a;
        second = &b;
    }
    else if ( a.available() == 0 && b.available() == size ) {
        first = &a;
        second = &b;
    }
    else if ( a.available() == size && b.available() == 0 ) {
        first = &b;
        second = &a;
    }
    else if ( a.available() > 0 && b.available() == 0 ) {
        first = &a;
        second = &b;
    }
    else if ( a.available() == 0 && b.available() > 0 ) {
        first = &b;
        second = &a;
    }
    else {
        Serial.printf("ERROR(4.4): wrong state, n: %d, a.available: %d, b.available: %d\n", n, a.available(), b.available());
        a.reset();
        b.reset();
        return false;
    }

    if ( first->roomLeft() >= n ) {
        return first->add(in, n);
    }

    // here we have n > first.roomLeft()
    const int copyToFirst = first->roomLeft();
    const int copyToSecond = n - first->roomLeft();

    const bool ok1 = first->add(in, copyToFirst);
    if ( !ok1 ) {
        Serial.printf("ERROR(5): add1() failed, n:%d, copyToFirst:%d, copyToSecond:%d\n", n, copyToFirst, copyToSecond);
        return false;
    }

    const bool ok2 = second->add(in + copyToFirst, copyToSecond);
    if ( !ok2 ) {
        Serial.printf("ERROR(6): add2() failed, n:%d, copyToFirst:%d, copyToSecond:%d\n", n, copyToFirst, copyToSecond);
        return false;
    }

    return true;
}

bool Chopper::get(uint8_t* inout) {
    if ( a.available() == size ) {
        return a.get(inout);
    }
    
    if ( b.available() == size ) {
        return b.get(inout);
    }
    
    Serial.printf("ERROR(7): a,b are both unavailable\n");

    return false;
}

bool Chopper::available() {
    return a.available() == size || b.available() == size;
}

int Chopper::roomLeft() {
    return a.roomLeft() + b.roomLeft();
}

void Chopper::printDebugInfo(const char* tag) {
    Serial.printf("tag:%s, size:%d, a.available:%d, b.available:%d\n", tag, size, a.available(), b.available());
}

