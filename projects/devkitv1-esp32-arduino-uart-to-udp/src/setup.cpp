#include <Arduino.h>
#include <WiFi.h>
#include <AsyncUDP_WT32_ETH01.h>
#include <chopper.hpp>

// GPIO_2 is connected into the blue LED in the board
const int GPIO_BLUE_LED_PIN = 2;
const int GPIO_SELECT_WIFI_MODE_PIN = 27;
const int GPIO_TELEMETRY_OUTPUT_CONTROL_PIN = 14;

const char* ap_ssid = "drone-1";
const char* ap_password = "noelia2034";
const bool enable_long_range = true;
const IPAddress ap_IP = IPAddress(192, 168, 2, 67);
const IPAddress sta_IP = IPAddress(192, 168, 2, 66);
const IPAddress gateway = IPAddress(192, 168, 2, 1);
const IPAddress subnet = IPAddress(255, 255, 255, 0);
const int server_udp_port = 20001;

typedef enum { 
    STA_MODE, 
    AP_MODE, 
} WifiModeEnum;

typedef enum { 
    BLINK_OFF, 
    BLINK_ON, 
    BLINK_FAST, 
    BLINK_SLOW, 
    BLINK_BEACON 
} BlinkEnum;

typedef enum { 
    STA_STATE_INIT, 
    STA_STATE_CONFIGURED,   
    STA_STATE_FAILED,
} STAStateEnum;

const char* staName ( STAStateEnum stat ) {
    if ( stat == STA_STATE_FAILED ) return "STA_STATE_FAILED";
    if ( stat == STA_STATE_INIT ) return "STA_STATE_INIT";
    if ( stat == STA_STATE_CONFIGURED ) return "STA_STATE_CONFIGURED";
    return "invalid";
}

const char* eventName ( WiFiEvent_t ev ) {
    if ( ev == SYSTEM_EVENT_WIFI_READY ) return "SYSTEM_EVENT_WIFI_READY";
    if ( ev == SYSTEM_EVENT_STA_START ) return "SYSTEM_EVENT_STA_START";
    if ( ev == SYSTEM_EVENT_STA_STOP ) return "SYSTEM_EVENT_STA_STOP";
    if ( ev == SYSTEM_EVENT_STA_CONNECTED ) return "SYSTEM_EVENT_STA_CONNECTED";
    if ( ev == SYSTEM_EVENT_STA_DISCONNECTED ) return "SYSTEM_EVENT_STA_DISCONNECTED";
    if ( ev == SYSTEM_EVENT_STA_AUTHMODE_CHANGE ) return "SYSTEM_EVENT_STA_AUTHMODE_CHANGE";
    if ( ev == SYSTEM_EVENT_STA_GOT_IP ) return "SYSTEM_EVENT_STA_GOT_IP";
    return "invalid";
}

const char* wifiStateName() {
    int stat = WiFi.status();
    if ( stat == WL_NO_SHIELD ) return "WL_NO_SHIELD";
    if ( stat == WL_IDLE_STATUS ) return "WL_IDLE_STATUS";
    if ( stat == WL_NO_SSID_AVAIL ) return "WL_NO_SSID_AVAIL";
    if ( stat == WL_SCAN_COMPLETED ) return "WL_SCAN_COMPLETED";
    if ( stat == WL_CONNECTED ) return "WL_CONNECTED";
    if ( stat == WL_CONNECT_FAILED ) return "WL_CONNECT_FAILED";
    if ( stat == WL_CONNECTION_LOST ) return "WL_CONNECTION_LOST";
    if ( stat == WL_DISCONNECTED ) return "WL_DISCONNECTED";
    return "invalid";
}

static AsyncUDP sta_mode_async_udp;
volatile STAStateEnum sta_state = STA_STATE_INIT;
volatile BlinkEnum blink_control_enum = BLINK_OFF;

bool initWifiAPMode() {
  
    // investigate: setting this to true apparently disables wifi capabilities permanently!!
    WiFi.enableLongRange(enable_long_range);

    if ( ! WiFi.mode(WIFI_AP) ) {
        return false;
    }
    
    if ( ! WiFi.softAP(ap_ssid, ap_password, 1, 0, 4) ) {
        return false;
    }

    if ( ! WiFi.softAPConfig(ap_IP, gateway, subnet) ) {
        return false;
    }

    Serial.printf("AP_MODE, ap_ssid:%s, localIP:%s, softAPIP:%s, softAPBroadcastIP:%s\n", 
        ap_ssid, 
        WiFi.localIP().toString().c_str(), 
        WiFi.softAPIP().toString().c_str(), 
        WiFi.softAPBroadcastIP().toString().c_str() 
    );

    static AsyncUDP ap_mode_async_udp;
    if ( ! ap_mode_async_udp.listen(ap_IP, server_udp_port) ) {
        return false;
    }

    static int loop_count = 0;
    static int read_count = 0;
    static int write_count = 0;

    ap_mode_async_udp.onPacket([](AsyncUDPPacket packet) {
       
        read_count = read_count + packet.length();

        // output destination control for AP mode only,
        // send telemetry data into Serial (the USB port) by grounding pin GPIO 14 
        // only read at startup time
        if ( digitalRead(GPIO_TELEMETRY_OUTPUT_CONTROL_PIN) == 0 ) {

            const int n_write = Serial.write(packet.data(), packet.length());
            write_count = write_count + packet.length();

            if ( n_write != packet.length() ) {
                blink_control_enum = BLINK_FAST;
            }
            else {
                blink_control_enum = BLINK_BEACON;
            }
        }
        else {
            blink_control_enum = BLINK_BEACON;
            if ( loop_count % 100 == 0 ) {
                Serial.printf(
                    "AP_MODE: loop_count:%8d, read_count:%8d, write_count:%8d, lost_percentage:%.3f, RRSI:%d\n", 
                    loop_count, 
                    read_count, 
                    write_count, 
                    100.0*(read_count-write_count)/(float)read_count,
                    WiFi.RSSI()
                );
            }
        }

        loop_count++;
    });

    return true;
}

void IRAM_ATTR handleLedIndication() {
    static int interrupt_count = 0;
    interrupt_count++;
    int scaled_count = interrupt_count / 5;
    switch ( blink_control_enum ) {
        case BLINK_OFF:
            digitalWrite(GPIO_BLUE_LED_PIN, LOW);
            break;
        case BLINK_ON:
            digitalWrite(GPIO_BLUE_LED_PIN, HIGH);
            break;
        case BLINK_BEACON:
            digitalWrite(GPIO_BLUE_LED_PIN, ((scaled_count)%30) == 0 ? HIGH : LOW);
            break;
        case BLINK_FAST:
            digitalWrite(GPIO_BLUE_LED_PIN, ((scaled_count)%2) == 0 ? HIGH : LOW);
            break;
        case BLINK_SLOW:
            digitalWrite(GPIO_BLUE_LED_PIN, ((scaled_count/20)%2) == 0 ? HIGH : LOW);
            break;
    }
}

// called from main() we do not need/want to do anything in here,
void initVariant() {}

// called from main() after returning from setup(),
// but as we are not returning from setup(), then we do not need to do anything in here
// as it is never reached,
void loop() {}

void transferFromUARTToUDP() {

    const int udpPacketSize = 512;
    static Chopper chopper(udpPacketSize);
    static uint8_t rx_buffer[udpPacketSize] = {0};

    static int loop_count = 0;
    static int total_write_count_before_udp = 0;
    static int total_write_count_after_udp = 0;
    static int total_read_count = 0;

    const int n_read = Serial2.read(rx_buffer, sizeof(rx_buffer));
    if ( n_read == 0 ) {
        return;
    }

    total_read_count = total_read_count + n_read;

    if ( n_read > chopper.roomLeft() ) {
         Serial.printf("STA_MODE: overflow, loop_count:%8d, n_read:%8d\n", loop_count, n_read);
         return;
    }

    if ( !chopper.add(rx_buffer, n_read) ) {
         Serial.printf("STA_MODE: chopper.add() failed, loop_count:%8d, n_read:%8d\n", loop_count, n_read);
         return;
    }
    
    if ( !chopper.available() ) {
        // this is a frequent condition
        return;
    }

    // chopper.available() is true if we got here
    // that guarantees that get() will fill in exactly udpPacketSize inside rx_buffer 
    if ( !chopper.get(rx_buffer) ) {
        Serial.printf("STA_MODE: chopper.get() failed, loop_count:%8d, n_read:%8d\n", loop_count, n_read);
        return;
    }

    const size_t n_sent = sta_mode_async_udp.write(rx_buffer, udpPacketSize);
    blink_control_enum = ( n_sent == udpPacketSize ) ? BLINK_BEACON : BLINK_FAST;

    total_write_count_before_udp = total_write_count_before_udp + udpPacketSize;
    total_write_count_after_udp = total_write_count_after_udp + n_sent;

    loop_count++;

    const int delta1 = total_read_count - total_write_count_before_udp;
    const int delta2 = total_write_count_before_udp - total_write_count_after_udp;

    if ( loop_count % 100 == 0 ) {
        Serial.printf(
            "STA_MODE: loop_count:%8d, RSSI:%d, n_read:%08d, total_read:%08d, before_udp:%08d, after_udp:%08d, delta1:%08d, delta2:%08d\n", 
            loop_count, 
            WiFi.RSSI(),
            n_read,
            total_read_count,
            total_write_count_before_udp, 
            total_write_count_after_udp,
            delta1,
            delta2 );
    }
}

void loopInSTAMode() {
    switch ( sta_state ) {
        case STA_STATE_INIT: {
            Serial.printf("loop() -- HANDLING state:%s, wifi_state:%s\n", staName(sta_state), wifiStateName());            

            {
                WiFi.enableLongRange(enable_long_range);
                const bool ok1 = WiFi.mode(WIFI_STA);
                Serial.printf( "Wifi in STA_MODE mode() %s\n", ok1 ? "succeeded" : "failed");
                if ( !ok1 ) {
                    break;
                }
            }

            {
                const bool ok2 =  WiFi.begin(ap_ssid, ap_password);
                Serial.printf( 
                    "Wifi in STA_MODE begin(%s, %s) %s\n", 
                    ap_ssid,
                    ap_password,
                    ok2 ? "succeeded" : "failed");
                if ( !ok2 ) {
                    break;
                }
            }

            while ( WiFi.status() != WL_CONNECTED ) {
                Serial.printf("not connected yet, wifi status: %s, waiting one second...\n", wifiStateName());
                delay(1000);
            }
            
            {
                const bool ok3 = WiFi.config(sta_IP, gateway, subnet); 
                Serial.printf( 
                    "Wifi in STA_MODE config(%s, %s, %s) %s\n", 
                    sta_IP.toString().c_str(),
                    gateway.toString().c_str(),
                    subnet.toString().c_str(),
                    ok3 ? "succeeded" : "failed");
                if ( !ok3 ) {
                    break;
                }                    
            }

            {
                const bool ok4 = sta_mode_async_udp.connect(ap_IP, server_udp_port);
                Serial.printf( 
                    "Wifi in STA_MODE async_udp(%s, %d) %s\n", 
                    ap_IP.toString().c_str(),
                    server_udp_port,
                    ok4 ? "succeeded" : "failed");
                if ( !ok4 ) {
                    break;
                }   
            }

            sta_state = STA_STATE_CONFIGURED;

            break;
        }

        case STA_STATE_FAILED: {
            Serial.printf("loop() -- IGNORING state:%s, wifi_state:%s\n", staName(sta_state), wifiStateName());            
            delay(1000);
            break;
        }

        case STA_STATE_CONFIGURED: {
            transferFromUARTToUDP();
            break;
        }
        
        default: {
            Serial.printf("loop() -- IGNORING unknown state:%d, wifi_state:%d\n", sta_state, WiFi.status());
            sta_state = STA_STATE_FAILED;
            delay(1000);
            break;
        }
    }
}

void IRAM_ATTR wifiSTAEventCb(WiFiEvent_t event) {
    switch ( event ) {
        case SYSTEM_EVENT_WIFI_READY:
        case SYSTEM_EVENT_STA_START: 
        case SYSTEM_EVENT_STA_CONNECTED: 
        case SYSTEM_EVENT_STA_GOT_IP:{
            Serial.printf("[WiFi-event] ignoring event %s\n", eventName(event));
            break;
        }
        case SYSTEM_EVENT_STA_DISCONNECTED:{
            if ( sta_state == STA_STATE_INIT ) {
                Serial.printf("[WiFi-event] ignoring event SYSTEM_EVENT_STA_DISCONNECTED\n");
            }
            else {
                Serial.printf("[WiFi-event] handling event SYSTEM_EVENT_STA_DISCONNECTED\n");
                sta_state = STA_STATE_INIT;
            }
            break;
        }
        default:{
            Serial.printf("[WiFi-event] handking unexpected event: %d\n", event);
            sta_state = STA_STATE_FAILED;
            break;
        }
    }
}

void setupTransferUARTUDP() {
    // Serial2 RX/TX UART: GPIO16/17
    // in mode STA: Serial2 is receiving data from a UART in STM32
    // in mode AP: Serial2 is not used
    Serial2.begin(921600);

    // setup the blue onboard LED 
    pinMode(GPIO_BLUE_LED_PIN, OUTPUT);
    digitalWrite(GPIO_BLUE_LED_PIN, LOW);

    // control pins should indicate logic=1 if not grounded
    pinMode(GPIO_SELECT_WIFI_MODE_PIN, INPUT_PULLUP);
    pinMode(GPIO_TELEMETRY_OUTPUT_CONTROL_PIN, INPUT_PULLUP);

    // indicate this way that things are still pending 
    // (1) select tick period to be 100uSec, f=10KHz, from fClock=80MHz => divisor=8000
    hw_timer_t* timer = timerBegin(0, 8000, true);
    // (2) hit on callback onTimer() every 10mSec, so then 1 out of 100 ticks
    timerAlarmWrite(timer, 100, true);
    timerAttachInterrupt(timer, &handleLedIndication, true);
    // (3) wait to enable the led handler timer..

    // WiFi mode selection:
    //    pin GPIO 27 grounded: STA mode
    //    pin GPIO 27 ploating: AP mode
    const WifiModeEnum wifiMode = digitalRead(GPIO_SELECT_WIFI_MODE_PIN) == 0 ? STA_MODE : AP_MODE;

    if ( wifiMode == STA_MODE ) {
        sta_state = STA_STATE_INIT;
        WiFi.onEvent(wifiSTAEventCb);
        blink_control_enum = BLINK_SLOW;
        // enable timer, but only after the wifi connection was established
        timerAlarmEnable(timer);
        while (true) {
            loopInSTAMode();
        }
    }

    if ( wifiMode == AP_MODE ) {
        const bool ok = initWifiAPMode();
        Serial.printf( "Wifi in AP_MODE mode initialization %s\n", ok ? "succeeded" : "failed");
        blink_control_enum = ok ? BLINK_SLOW : BLINK_FAST;
        // enable timer, but only after the wifi connection was established
        timerAlarmEnable(timer);
        while (true) {
            // do nothing, all is driven by callbacks
        }
    }

    Serial.println("initialization error");
}

void setupSerialDebugLine() {
    // Serial RX/TX UART: USB UART
    // in mode STA: optionally connected to the computer, all debugging messages from this program always go into Serial
    // in mode AP: to be connected to the computer, in both cases below all debugging messages from this always program goes into Serial
    //   if control pin GPIO 14 is grounded: the telemetry data received from STA over WiFi is repeated into Serial
    //   if control pin GPIO 14 is floating: the telemetry data received from STA over WiFi is not send anywhere and therefore dropped
    Serial.begin(921600);
  
    Serial.println("\n\n\nprogram started");
    Serial.printf("CPU clock freq: %d MHz\n", getCpuFrequencyMhz());
}

void setup() {
    setupSerialDebugLine();
    setupTransferUARTUDP();
}



