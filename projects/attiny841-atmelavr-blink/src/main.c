/**
 * Copyright (C) PlatformIO <contact@platformio.org>
 * See LICENSE for details.
 */

#include <avr/io.h>
#include <util/delay.h>

void toggleLEDPin() {
    static int ledOn = 0;

    ledOn = ( ledOn == 0 ) ? 1 : 0;
    if ( ledOn == 1 ) {
        PORTB |= 1 << PB2;
    }
    else {
        PORTB &= ~( 1 << PB2 );
    }
}

// make the LED pin an output for PB2
void initLEDPin() {
    DDRB |= (1 << DDB2);
}

int main()
{
    initLEDPin();
    while (1)
    {
        toggleLEDPin();
        _delay_ms(100);
    }
    return 0;
}
