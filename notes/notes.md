# Using ESP8266 from arduino 
----------------------------

Products:
  - ESP8266 NodeMCU V1.0 ESP-12E WiFI Module
  - ESP8266 Huzzah 
  - Arduino IDE
  - ESP8266 libraries for Arduino IDE
  - RS232-USB FTDI adapter

NodeMCU schematics 

https://circuit-diagramz.com/wp-content/uploads/2018/11/

ESP8266-12e-Pinout-Schematic-Circuit-Diagram-NodeMCU.pdf  

https://www.make-it.ca/nodemcu-arduino/nodemcu-details-specifications/

https://github.com/nodemcu/nodemcu-devkit-v1.0/blob/master/NODEMCU_DEVKIT_V1.0.PDF

https://github.com/nodemcu/nodemcu-devkit-v1.0

arduino IDE installation

https://www.arduino.cc/en/Guide

Arduino extension for esp8266
Install on Arduino IDE via Boards Manager

https://github.com/esp8266/Arduino

Videos & Tutorial

https://www.youtube.com/results?search_query=nodemcu 

NodeMCU from HiLetgo (bought it from Amazon 08/2020)

http://www.hiletgo.com/ProductDetail/1906570.html

Seems similar to this:

https://protosupplies.com/product/esp8266-nodemcu-v1-0-esp-12e-wifi-module/

Adafruit's Huzzah (usage, schematics, etc..)

https://learn.adafruit.com/adafruit-feather-huzzah-esp8266

Adafruit's Huzzah + MQTT example (Ladyada from Adafruit)

https://www.youtube.com/watch?v=VjpONmC2tac&t=2992s

NodeMCU Libraries

https://nodemcu.readthedocs.io

RS232-USB FTDI adapter

https://smile.amazon.com/gp/product/B07TXVRQ7V/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1

(from Ismayil)
beginners guide to ESP8266 

https://tttapa.github.io/ESP8266/Chap01%20-%20ESP8266.html

swiss guy on MQTT/esp2866

https://www.youtube.com/watch?v=9G-nMGcELG8

see comments with reference to code

platformio

https://docs.platformio.org/en/latest/what-is-platformio.html
https://platformio.org/

how to move out from arduino into PlatformIO 

https://blog.emtwo.ch/2020/05/

migrating-from-ardiuno-ide-to-visual.html

how to install libraries in platformio

https://www.youtube.com/watch?v=EBlHNBNHESQ

nodeMCU board info

https://www.youtube.com/watch?v=IHocU-VqsF0

platformio doc & user guide
https://docs.platformio.org/en/latest/tutorials

node MCU pinout and hardware side
https://www.youtube.com/watch?v=IHocU-VqsF0

platformio tutorial 
https://www.youtube.com/watch?v=k3tpNwXEWhU

nodeMCU MQTT library
https://github.com/knolleary/pubsubclient

nodeMCU serial library 
platformio lib search -n serial  -> serial 4625

nodeMCU example (probably commercially biased)
https://www.losant.com/blog/getting-started-with-platformio-esp8266-nodemcu

library for mqtt (id:89)
https://github.com/knolleary/pubsubclient/blob/master/library.json

> pio lib search "id:89"
> pio lib search "platform: Espressif 8266 AND keyword:mqtt"
> pio lib search "platform: Espressif 8266 AND keyword:serial"
> pio lib search --framework arduino

ie:
platformio lib search "id:89 8266 AND keyword:mqtt"

Miscellaneous
-------------

all kind of gateways 

https://www.youtube.com/watch?v=TBvGrB094Co

jeelabs esp-link

https://www.youtube.com/watch?v=mtazgora9xE


pio useful commands:
View/Command Palette -> PlatformIO: Rebuild Intellisense Index

pio lib search -n mqtt 
pio settings get

day-to-day
pio run --target clean
pio run
pio run --target upload
pio device monitor
